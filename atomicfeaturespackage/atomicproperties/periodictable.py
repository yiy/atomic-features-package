#!/usr/bin/env python
# coding: utf-8

''' 
Module : periodictable

This module with help to visualize atomic properties available from dft, lda2015, matminer,webele and pymat module via the periodic table in the form of heatmaps 
'''

from bokeh.io import output_notebook, output_file,show, push_notebook
from bokeh.models import ColumnDataSource,CustomJS, HoverTool,ColorBar, NumeralTickFormatter,LinearColorMapper,Select
from bokeh.plotting import figure
from bokeh.sampledata.periodic_table import elements
from bokeh.transform import dodge, factor_cmap, linear_cmap
from bokeh.palettes import PRGn, RdYlGn,Blues8,Colorblind
from bokeh.layouts import column, layout,row
from ipywidgets import interact, Layout
import pandas as pd
import os
import numpy as np
import re
from atomicfeaturespackage.atomicproperties import atomic_properties_matminer as mm
from atomicfeaturespackage.atomicproperties import atomic_properties_pymat as pymatdf
from atomicfeaturespackage.atomicproperties import atomic_properties_webele as apwebele

# Here using already available dataframe in bokeh package element and modify it contents

# Use periods as y axis and groups as x axis for our plot
periods = ["I", "II", "III", "IV", "V", "VI", "VII"," ","LC","AC"]
groups = [str(x) for x in range(1, 19)]
pd.options.mode.chained_assignment = None


def clean():
    '''
    Cleaning and renaming of pandas dataframe imported from bokeh package
    '''
    df = elements.copy()
    df["atomic mass"] = df["atomic mass"].astype(str)
    df["group"] = df["group"].astype(str)
    df["period"] = [periods[x-1] for x in df.period]
    df1 = df[df.group != "-"]


    df = elements.copy()
    df["atomic mass"] = df["atomic mass"].astype(str)
    df["group"] = df["group"].astype(str)
    df["period"] = df["period"].astype(str)
    df["period"].replace(to_replace ="6",value ="LC",inplace = True)
    df["period"].replace(to_replace ="7",value ="AC", inplace = True)
    df2 = df[df.group == "-"]
    a = [*range(4,18,1),*range(4,18,1)]
    for count, obj in enumerate(a):
        df2.iloc[count,df2.columns.get_loc("group")] = str(obj)
    
    new = [df1, df2]
    comb = pd.concat(new)
    df3 = comb.drop(columns=['electronegativity', 'atomic radius','ion radius','van der Waals radius','IE-1','EA','standard state','bonding type','melting point','boiling point','density','year discovered'],inplace=False)
    df3['atomic mass'] = (df3['atomic mass'].str.replace(r"(\[)|(\])", '')).astype(float)
    globals()['df3'] = df3


def featuref():
    '''
    Generates the python list of features needed for interactive dropdown menu in heatmap plot. This functions gives features list for element properties from atomic_properties_dft module and spin set to "False".
    '''    
    all_features = list(dffa.columns)
    remove = ['symbol','name','CPK','electronic configuration','metal','group','period']
    Feature = []
    for element in all_features:
        if element not in remove:
            Feature.append(element)
    return Feature

def featuret():
    '''
    Generates the python list of features needed for interactive dropdown menu in heatmap plot. This functions gives features list for element properties from atomic_properties_dft module and spin set to "True".
    '''    
    all_features = list(dftr.columns)
    remove = ['symbol','name','CPK','electronic configuration','metal','group','period']
    Feature = []
    for element in all_features:
        if element not in remove:
            Feature.append(element)
    return Feature

def featurelda():
    '''
    Generates the python list of features needed for interactive dropdown menu in heatmap plot. This functions gives features list for element properties from atomic_properties_lda2015 module.
    '''    
    all_features = list(dflda.columns)
    remove = ['symbol','name','CPK','electronic configuration','metal','group','period']
    Feature = []
    for element in all_features:
        if element not in remove:
            Feature.append(element)
    return Feature

def featurematmin():
    '''
    Generates the python list of features needed for interactive dropdown menu in heatmap plot. This functions gives features list for element properties from atomic_properties_magpie module.
    '''    
    all_features = list(dfmatm.columns)
    remove = ['symbol','name','CPK','electronic configuration','metal','group','period','atomic_isnonmetal',
              'atomic_ismetalloid','atomic_ismetal','atomic_isfblock','atomic_isdblock','atomic_isalkali','atomic_ie',
              'atomic_oxstates','periodict_column','periodict_row']
    Feature = []
    for element in all_features:
        if element not in remove:
            Feature.append(element)
    return Feature

def featurepymat():
    '''
    Generates the python list of features needed for interactive dropdown menu in heatmap plot. This functions gives features list for element properties from atomic_properties_pymat module.
    '''    
    all_features = list(dfpymat.columns)
    remove = ['symbol','name','CPK','electronic configuration','metal','group','period',
              'ionic_radii_hs','ionic_radii_ls','icsd_oxd_states','atomic_refractive_index','shannon_radii',
             'ionic_radii','electronic_structure','common_ox_states','atomic_orbitals','oxidation_states',
              'nmr_quadrapole_mom']
    Feature = []
    for element in all_features:
        if element not in remove:
            Feature.append(element)
    return Feature
    
def featurewebele():
    '''
    Generates the python list of features needed for interactive dropdown menu in heatmap plot. This functions gives features list for element properties from atomic_properties_webele module.
    '''     
    all_features = list(dfwebele.columns)
    remove = ['symbol','CPK','electronic configuration','metal','group','period']
    Feature = []
    #globals()['Feature'] = []
    for element in all_features:
        if element not in remove:
            Feature.append(element)
    return Feature

def ptableplotgent(Feature):
    '''
    Sets all the heatmap plot properties like text, colour mappers ,tittle, axis, legend etc for atomic_properties_dft module(Spin="True").
    '''
    source = ColumnDataSource(dftr)
  
    p2 = figure(plot_width=950, plot_height=500,title="Periodic Table",
           x_range=groups, y_range=list(reversed(periods)), toolbar_location=None,tools="hover")
    
    
    if Feature == 'atomic_ea' :
        color_mapper = linear_cmap(field_name = 'atomic_ea', palette = Colorblind[8], 
                                   low = dftr['atomic_ea'].min(), 
                                   high = dftr['atomic_ea'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ea", "@atomic_ea eV"),
        ]
        
    if Feature == 'atomic_ip' :
        color_mapper = linear_cmap(field_name = 'atomic_ip', palette = Colorblind[8], 
                                   low = dftr['atomic_ip'].min(), 
                                   high = dftr['atomic_ip'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ip", "@atomic_ip eV"),
        ]
        
    if Feature == 'atomic number' :
        color_mapper = linear_cmap(field_name = 'atomic number', palette = Colorblind[8], 
                                   low = dftr['atomic number'].min(), 
                                   high = dftr['atomic number'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Atomic number", "@{atomic number}"),
        ("Type", "@metal"),
        ]
        
    if Feature == 'atomic mass' :
        color_mapper = linear_cmap(field_name = 'atomic mass', palette = Colorblind[8], 
                                   low = dftr['atomic mass'].min(), 
                                   high = dftr['atomic mass'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Atomic mass", "@{atomic mass} amu"),
        ("Type", "@metal")
        ]
        
    if Feature == 'atomic_hfomo' :
        color_mapper = linear_cmap(field_name = 'atomic_hfomo', palette = Colorblind[8], 
                                   low = dftr['atomic_hfomo'].min(), 
                                   high = dftr['atomic_hfomo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_hfomo", "@atomic_hfomo eV"),
        ]
    
    if Feature == 'atomic_hpomo' :
        color_mapper = linear_cmap(field_name = 'atomic_hpomo', palette = Colorblind[8], 
                                   low = dftr['atomic_hpomo'].min(), 
                                   high = dftr['atomic_hpomo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_hpomo", "@atomic_hpomo eV"),
        ]
        
    if Feature == 'atomic_lfumo' :
        color_mapper = linear_cmap(field_name = 'atomic_lfumo', palette = Colorblind[8], 
                                   low = dftr['atomic_lfumo'].min(), 
                                   high = dftr['atomic_lfumo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_lfumo", "@atomic_lfumo eV"),
        ]
        
    if Feature == 'atomic_hpumo' :
        color_mapper = linear_cmap(field_name = 'atomic_hpumo', palette = Colorblind[8], 
                                   low = dftr['atomic_hpumo'].min(), 
                                   high = dftr['atomic_hpumo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_lpumo", "@atomic_hpumo eV"),
        ]
        
    if Feature == 'atomic_lpumo' :
        color_mapper = linear_cmap(field_name = 'atomic_lpumo', palette = Colorblind[8], 
                                   low = dftr['atomic_lpumo'].min(), 
                                   high = dftr['atomic_lpumo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_lpumo", "@atomic_lpumo eV"),
        ]
        
    if Feature == 'atomic_ip_by_half_charged_homo' :
        color_mapper = linear_cmap(field_name = 'atomic_ip_by_half_charged_homo', palette = Colorblind[8], 
                                   low = dftr['atomic_ip_by_half_charged_homo'].min(), 
                                   high = dftr['atomic_ip_by_half_charged_homo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ip_by_half_charged_homo", "@atomic_ip_by_half_charged_homo eV"),
        ]
        
    if Feature == 'atomic_ea_by_half_charged_homo' :
        color_mapper = linear_cmap(field_name = 'atomic_ea_by_half_charged_homo', palette = Colorblind[8], 
                                   low = dftr['atomic_ea_by_half_charged_homo'].min(), 
                                   high = dftr['atomic_ea_by_half_charged_homo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ea_by_half_charged_homo", "@atomic_ea_by_half_charged_homo eV"),
        ]
        
    if Feature == 'atomic_r_up_s_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_s_neg_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_s_neg_1'].min(), 
                                   high = dftr['atomic_r_up_s_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_s_neg_1", "@atomic_r_up_s_neg_1 pm"),
        ]
        
    if Feature == 'atomic_r_dn_s_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_s_neg_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_s_neg_1'].min(), 
                                   high = dftr['atomic_r_dn_s_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_s_neg_1", "@atomic_r_dn_s_neg_1 pm"),
        ]
        
    if Feature == 'atomic_r_up_p_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_p_neg_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_p_neg_1'].min(), 
                                   high = dftr['atomic_r_up_p_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_p_neg_1", "@atomic_r_up_p_neg_1 pm"),
        ]
        
    if Feature == 'atomic_r_dn_p_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_p_neg_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_p_neg_1'].min(), 
                                   high = dftr['atomic_r_dn_p_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_p_neg_1", "@atomic_r_dn_p_neg_1 pm"),
        ]
        
    if Feature == 'atomic_r_dn_d_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_d_neg_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_d_neg_1'].min(), 
                                   high = dftr['atomic_r_dn_d_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_d_neg_1", "@atomic_r_dn_d_neg_1 pm"),
        ]
        
    if Feature == 'atomic_r_up_val_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_val_neg_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_val_neg_1'].min(), 
                                   high = dftr['atomic_r_up_val_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_val_neg_1", "@atomic_r_up_val_neg_1 pm"),
        ]
        
    if Feature == 'atomic_r_dn_val_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_val_neg_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_val_neg_1'].min(), 
                                   high = dftr['atomic_r_dn_val_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_val_neg_1", "@atomic_r_dn_val_neg_1 pm"),
        ]
        
    if Feature == 'atomic_r_up_s_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_s_neg_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_s_neg_05'].min(), 
                                   high = dftr['atomic_r_up_s_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_s_neg_05", "@atomic_r_up_s_neg_05 pm"),
        ]
        
    if Feature == 'atomic_r_dn_s_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_s_neg_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_s_neg_05'].min(), 
                                   high = dftr['atomic_r_dn_s_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_s_neg_05", "@atomic_r_dn_s_neg_05 pm"),
        ]
        
    if Feature == 'atomic_r_up_p_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_p_neg_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_p_neg_05'].min(), 
                                   high = dftr['atomic_r_up_p_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_p_neg_05", "@atomic_r_up_p_neg_05 pm"),
        ]
        
    if Feature == 'atomic_r_dn_p_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_p_neg_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_p_neg_05'].min(), 
                                   high = dftr['atomic_r_dn_p_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_p_neg_05", "@atomic_r_dn_p_neg_05 pm"),
        ]
        
    if Feature == 'atomic_r_up_d_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_d_neg_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_d_neg_05'].min(), 
                                   high = dftr['atomic_r_up_d_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_d_neg_05", "@atomic_r_up_d_neg_05 pm"),
        ]
        
    if Feature == 'atomic_r_dn_d_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_d_neg_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_d_neg_05'].min(), 
                                   high = dftr['atomic_r_dn_d_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_d_neg_05", "@atomic_r_dn_d_neg_05 pm"),
        ]
        
    if Feature == 'atomic_r_up_val_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_val_neg_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_val_neg_05'].min(), 
                                   high = dftr['atomic_r_up_val_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_val_neg_05", "@atomic_r_up_val_neg_05 pm"),
        ]

    if Feature == 'atomic_r_dn_val_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_val_neg_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_val_neg_05'].min(), 
                                   high = dftr['atomic_r_dn_val_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_val_neg_05", "@atomic_r_dn_val_neg_05 pm"),
        ]

    if Feature == 'atomic_r_up_s' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_s', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_s'].min(), 
                                   high = dftr['atomic_r_up_s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_s", "@atomic_r_up_s pm"),
        ]

    if Feature == 'atomic_r_dn_s' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_s', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_s'].min(), 
                                   high = dftr['atomic_r_dn_s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_s", "@atomic_r_dn_s pm"),
        ]

    if Feature == 'atomic_r_up_p' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_p', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_p'].min(), 
                                   high = dftr['atomic_r_up_p'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_p", "@atomic_r_up_p pm"),
        ]

    if Feature == 'atomic_r_dn_p' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_p', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_p'].min(), 
                                   high = dftr['atomic_r_dn_p'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_p", "@atomic_r_dn_p pm"),
        ]

    if Feature == 'atomic_r_up_d' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_d', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_d'].min(), 
                                   high = dftr['atomic_r_up_d'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_d", "@atomic_r_up_d pm"),
        ]

    if Feature == 'atomic_r_dn_d' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_d', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_d'].min(), 
                                   high = dftr['atomic_r_dn_d'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_d", "@atomic_r_dn_d pm"),
        ]

    if Feature == 'atomic_r_up_val' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_val', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_val'].min(), 
                                   high = dftr['atomic_r_up_val'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_val", "@atomic_r_up_val pm"),
        ]

    if Feature == 'atomic_r_dn_val' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_val', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_val'].min(), 
                                   high = dftr['atomic_r_dn_val'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_val", "@atomic_r_dn_val pm"),
        ]

    if Feature == 'atomic_r_up_s_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_s_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_s_05'].min(), 
                                   high = dftr['atomic_r_up_s_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_s_05", "@atomic_r_up_s_05 pm"),
        ]

    if Feature == 'atomic_r_dn_s_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_s_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_s_05'].min(), 
                                   high = dftr['atomic_r_dn_s_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_s_05", "@atomic_r_dn_s_05 pm"),
        ]

    if Feature == 'atomic_r_up_p_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_p_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_p_05'].min(), 
                                   high = dftr['atomic_r_up_p_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_p_05", "@atomic_r_up_p_05 pm"),
        ]

    if Feature == 'atomic_r_dn_p_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_p_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_p_05'].min(), 
                                   high = dftr['atomic_r_dn_p_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_p_05", "@atomic_r_dn_p_05 pm"),
        ]

    if Feature == 'atomic_r_up_d_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_d_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_d_05'].min(), 
                                   high = dftr['atomic_r_up_d_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_d_05", "@atomic_r_up_d_05 pm"),
        ]

    if Feature == 'atomic_r_dn_d_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_d_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_d_05'].min(), 
                                   high = dftr['atomic_r_dn_d_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_d_05", "@atomic_r_dn_d_05 pm"),
        ]

    if Feature == 'atomic_r_up_val_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_val_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_val_05'].min(), 
                                   high = dftr['atomic_r_up_val_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_val_05", "@atomic_r_up_val_05 pm"),
        ]

    if Feature == 'atomic_r_dn_val_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_val_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_val_05'].min(), 
                                   high = dftr['atomic_r_dn_val_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_val_05", "@atomic_r_dn_val_05 pm"),
        ]

    if Feature == 'atomic_r_up_s_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_s_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_s_1'].min(), 
                                   high = dftr['atomic_r_up_s_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_s_1", "@atomic_r_up_s_1 pm"),
        ]

    if Feature == 'atomic_r_dn_s_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_s_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_s_1'].min(), 
                                   high = dftr['atomic_r_dn_s_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_s_1", "@atomic_r_dn_s_1 pm"),
        ]

    if Feature == 'atomic_r_up_p_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_p_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_p_1'].min(), 
                                   high = dftr['atomic_r_up_p_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_p_1", "@atomic_r_up_p_1 pm"),
        ]

    if Feature == 'atomic_r_dn_p_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_p_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_p_1'].min(), 
                                   high = dftr['atomic_r_dn_p_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_p_1", "@atomic_r_dn_p_1 pm"),
        ]

    if Feature == 'atomic_r_up_d_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_d_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_d_1'].min(), 
                                   high = dftr['atomic_r_up_d_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_d_1", "@atomic_r_up_d_1 pm"),
        ]

    if Feature == 'atomic_r_dn_d_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_d_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_d_1'].min(), 
                                   high = dftr['atomic_r_dn_d_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_d_1", "@atomic_r_dn_d_1 pm"),
        ]

    if Feature == 'atomic_r_up_val_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_val_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_val_1'].min(), 
                                   high = dftr['atomic_r_up_val_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_val_1", "@atomic_r_up_val_1 pm"),
        ]

    if Feature == 'atomic_r_dn_val_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_val_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_val_1'].min(), 
                                   high = dftr['atomic_r_dn_val_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_val_1", "@atomic_r_dn_val_1 pm"),
        ]

        
    
    p2.rect("group", "period", 0.90, 0.90, source= source, fill_alpha=0.6,color=color_mapper)
    text_props = {"source":source, "text_align": "left", "text_baseline": "middle", "text_color" : "black"}

    x1 = dodge("group", -0.4, range=p2.x_range)

    r1 = p2.text(x=x1, y="period", text="symbol", **text_props)
    r1.glyph.text_font_style="bold"

    r1 = p2.text(x=x1, y=dodge("period", 0.3, range=p2.y_range), text="atomic number", **text_props)
    r1.glyph.text_font_size="8pt"

    r1 = p2.text(x=x1, y=dodge("period", -0.35, range=p2.y_range), text="name", **text_props)
    r1.glyph.text_font_size="5pt"
    #Defines color bar
    color_bar = ColorBar(color_mapper=color_mapper['transform'],
                         formatter = NumeralTickFormatter(format='0.0000'),label_standoff = 13, width=8, location=(0,0))
    # Set color_bar location
    p2.add_layout(color_bar, 'right')
    p2.outline_line_color = None
    p2.grid.grid_line_color = None
    p2.axis.axis_line_color = None
    p2.axis.major_tick_line_color = None
    p2.axis.major_label_standoff = 0
    p2.axis.visible = False
    output_notebook(hide_banner=True)
    show(p2,notebook_handle=True)
    push_notebook()


def ptableplotgenf(Feature):
    '''
    Sets all the heatmap plot properties like text, colour mappers ,tittle, axis, legend etc for atomic_properties_dft module(Spin="False").
    '''
    source = ColumnDataSource(dffa)
  
    p2 = figure(plot_width=950, plot_height=500,title="Periodic Table",
           x_range=groups, y_range=list(reversed(periods)), toolbar_location=None,tools="hover")
    
    
    if Feature == 'atomic_ea' :
        color_mapper = linear_cmap(field_name = 'atomic_ea', palette = Colorblind[8], 
                                   low = dffa['atomic_ea'].min(), 
                                   high = dffa['atomic_ea'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ea", "@atomic_ea eV"),
        ]
        
    if Feature == 'atomic_ip' :
        color_mapper = linear_cmap(field_name = 'atomic_ip', palette = Colorblind[8], 
                                   low = dffa['atomic_ip'].min(), 
                                   high = dffa['atomic_ip'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ip", "@atomic_ip eV"),
        ]
        
    if Feature == 'atomic number' :
        color_mapper = linear_cmap(field_name = 'atomic number', palette = Colorblind[8], 
                                   low = dffa['atomic number'].min(), 
                                   high = dffa['atomic number'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Atomic number", "@{atomic number}"),
        ("Type", "@metal"),
        ]

        
    if Feature == 'atomic mass' :
        color_mapper = linear_cmap(field_name = 'atomic mass', palette = Colorblind[8], 
                                   low = dffa['atomic mass'].min(), 
                                   high = dffa['atomic mass'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Atomic mass", "@{atomic mass} amu"),
        ("Type", "@metal")
        ]

        
    if Feature == 'atomic_hfomo' :
        color_mapper = linear_cmap(field_name = 'atomic_hfomo', palette = Colorblind[8], 
                                   low = dffa['atomic_hfomo'].min(), 
                                   high = dffa['atomic_hfomo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_hfomo", "@atomic_hfomo eV"),
        ]

    
    if Feature == 'atomic_hpomo' :
        color_mapper = linear_cmap(field_name = 'atomic_hpomo', palette = Colorblind[8], 
                                   low = dffa['atomic_hpomo'].min(), 
                                   high = dffa['atomic_hpomo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_hpomo", "@atomic_hpomo eV"),
        ]

        
    if Feature == 'atomic_lfumo' :
        color_mapper = linear_cmap(field_name = 'atomic_lfumo', palette = Colorblind[8], 
                                   low = dffa['atomic_lfumo'].min(), 
                                   high = dffa['atomic_lfumo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_lfumo", "@atomic_lfumo eV"),
        ]

        
    if Feature == 'atomic_hpumo' :
        color_mapper = linear_cmap(field_name = 'atomic_hpumo', palette = Colorblind[8], 
                                   low = dffa['atomic_hpumo'].min(), 
                                   high = dffa['atomic_hpumo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_lpumo", "@atomic_hpumo eV"),
        ]

        
    if Feature == 'atomic_lpumo' :
        color_mapper = linear_cmap(field_name = 'atomic_lpumo', palette = Colorblind[8], 
                                   low = dffa['atomic_lpumo'].min(), 
                                   high = dffa['atomic_lpumo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_lpumo", "@atomic_lpumo eV"),
        ]

        
    if Feature == 'atomic_ip_by_half_charged_homo' :
        color_mapper = linear_cmap(field_name = 'atomic_ip_by_half_charged_homo', palette = Colorblind[8], 
                                   low = dffa['atomic_ip_by_half_charged_homo'].min(), 
                                   high = dffa['atomic_ip_by_half_charged_homo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ip_by_half_charged_homo", "@atomic_ip_by_half_charged_homo eV"),
        ]

        
    if Feature == 'atomic_ea_by_half_charged_homo' :
        color_mapper = linear_cmap(field_name = 'atomic_ea_by_half_charged_homo', palette = Colorblind[8], 
                                   low = dffa['atomic_ea_by_half_charged_homo'].min(), 
                                   high = dffa['atomic_ea_by_half_charged_homo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ea_by_half_charged_homo", "@atomic_ea_by_half_charged_homo eV"),
        ]

        
    #non spin features
        
    if Feature == 'atomic_r_s_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_s_neg_1', palette = Colorblind[8], 
                                   low = dffa['atomic_r_s_neg_1'].min(), 
                                   high = dffa['atomic_r_s_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_s_neg_1", "@atomic_r_s_neg_1 pm"),
        ]

    if Feature == 'atomic_r_p_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_p_neg_1', palette = Colorblind[8], 
                                   low = dffa['atomic_r_p_neg_1'].min(), 
                                   high = dffa['atomic_r_p_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_p_neg_1", "@atomic_r_p_neg_1 pm"),
        ]

    if Feature == 'atomic_r_d_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_d_neg_1', palette = Colorblind[8], 
                                   low = dffa['atomic_r_d_neg_1'].min(), 
                                   high = dffa['atomic_r_d_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_d_neg_1", "@atomic_r_d_neg_1 pm"),
        ]

    if Feature == 'atomic_r_val_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_val_neg_1', palette = Colorblind[8], 
                                   low = dffa['atomic_r_val_neg_1'].min(), 
                                   high = dffa['atomic_r_val_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_val_neg_1", "@atomic_r_val_neg_1 pm"),
        ]

    if Feature == 'atomic_r_s_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_s_neg_05', palette = Colorblind[8], 
                                   low = dffa['atomic_r_s_neg_05'].min(), 
                                   high = dffa['atomic_r_s_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_s_neg_05", "@atomic_r_s_neg_05 pm"),
        ]

    if Feature == 'atomic_r_p_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_p_neg_05', palette = Colorblind[8], 
                                   low = dffa['atomic_r_p_neg_05'].min(), 
                                   high = dffa['atomic_r_p_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_p_neg_05", "@atomic_r_p_neg_05 pm"),
        ]

    if Feature == 'atomic_r_d_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_d_neg_05', palette = Colorblind[8], 
                                   low = dffa['atomic_r_d_neg_05'].min(), 
                                   high = dffa['atomic_r_d_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_d_neg_05", "@atomic_r_d_neg_05 pm"),
        ]
 
    if Feature == 'atomic_r_val_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_val_neg_05', palette = Colorblind[8], 
                                   low = dffa['atomic_r_val_neg_05'].min(), 
                                   high = dffa['atomic_r_val_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_val_neg_05", "@atomic_r_val_neg_05 pm"),
        ]

    if Feature == 'atomic_r_s' :
        color_mapper = linear_cmap(field_name = 'atomic_r_s', palette = Colorblind[8], 
                                   low = dffa['atomic_r_s'].min(), 
                                   high = dffa['atomic_r_s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_s", "@atomic_r_s pm"),
        ]

    if Feature == 'atomic_r_p' :
        color_mapper = linear_cmap(field_name = 'atomic_r_p', palette = Colorblind[8], 
                                   low = dffa['atomic_r_p'].min(), 
                                   high = dffa['atomic_r_p'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_p", "@atomic_r_p pm"),
        ]
 
    if Feature == 'atomic_r_d' :
        color_mapper = linear_cmap(field_name = 'atomic_r_d', palette = Colorblind[8], 
                                   low = dffa['atomic_r_d'].min(), 
                                   high = dffa['atomic_r_d'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_d", "@atomic_r_d pm"),
        ]

    if Feature == 'atomic_r_val' :
        color_mapper = linear_cmap(field_name = 'atomic_r_val', palette = Colorblind[8], 
                                   low = dffa['atomic_r_val'].min(), 
                                   high = dffa['atomic_r_val'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_val", "@atomic_r_val pm"),
        ]

    if Feature == 'atomic_r_s_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_s_05', palette = Colorblind[8], 
                                   low = dffa['atomic_r_s_05'].min(), 
                                   high = dffa['atomic_r_s_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_s_05", "@atomic_r_s_05 pm"),
        ]
 
    if Feature == 'atomic_r_p_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_p_05', palette = Colorblind[8], 
                                   low = dffa['atomic_r_p_05'].min(), 
                                   high = dffa['atomic_r_p_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_p_05", "@atomic_r_p_05 pm"),
        ]
 
    if Feature == 'atomic_r_d_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_d_05', palette = Colorblind[8], 
                                   low = dffa['atomic_r_d_05'].min(), 
                                   high = dffa['atomic_r_d_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_d_05", "@atomic_r_d_05 pm"),
        ]

    if Feature == 'atomic_r_val_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_val_05', palette = Colorblind[8], 
                                   low = dffa['atomic_r_val_05'].min(), 
                                   high = dffa['atomic_r_val_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_val_05", "@atomic_r_val_05 pm"),
        ]

    if Feature == 'atomic_r_s_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_s_1', palette = Colorblind[8], 
                                   low = dffa['atomic_r_s_1'].min(), 
                                   high = dffa['atomic_r_s_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_s_1", "@atomic_r_s_1 pm"),
        ]

    if Feature == 'atomic_r_p_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_p_1', palette = Colorblind[8], 
                                   low = dffa['atomic_r_p_1'].min(), 
                                   high = dffa['atomic_r_p_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_p_1", "@atomic_r_p_1 pm"),
        ]

    if Feature == 'atomic_r_d_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_d_1', palette = Colorblind[8], 
                                   low = dffa['atomic_r_d_1'].min(), 
                                   high = dffa['atomic_r_d_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_d_1", "@atomic_r_d_1 pm"),
        ]

    if Feature == 'atomic_r_val_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_val_1', palette = Colorblind[8], 
                                   low = dffa['atomic_r_val_1'].min(), 
                                   high = dffa['atomic_r_val_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_val_1", "@atomic_r_val_1 pm"),
        ]
 
    p2.rect("group", "period", 0.90, 0.90, source= source, fill_alpha=0.6,color=color_mapper)
    text_props = {"source":source, "text_align": "left", "text_baseline": "middle", "text_color" : "black"}

    x1 = dodge("group", -0.4, range=p2.x_range)

    r1 = p2.text(x=x1, y="period", text="symbol", **text_props)
    r1.glyph.text_font_style="bold"

    r1 = p2.text(x=x1, y=dodge("period", 0.3, range=p2.y_range), text="atomic number", **text_props)
    r1.glyph.text_font_size="8pt"

    r1 = p2.text(x=x1, y=dodge("period", -0.35, range=p2.y_range), text="name", **text_props)
    r1.glyph.text_font_size="5pt"
    #Defines color bar
    color_bar = ColorBar(color_mapper=color_mapper['transform'],
                         formatter = NumeralTickFormatter(format='0.0000'),label_standoff = 13, width=8, location=(0,0))
    # Set color_bar location
    p2.add_layout(color_bar, 'right')
    p2.outline_line_color = None
    p2.grid.grid_line_color = None
    p2.axis.axis_line_color = None
    p2.axis.major_tick_line_color = None
    p2.axis.major_label_standoff = 0
    p2.axis.visible = False
    output_notebook(hide_banner=True)
    show(p2,notebook_handle=True)
    push_notebook()

def ptableplotgenlda(Feature):
    '''
    Sets all the heatmap plot properties like text, colour mappers ,tittle, axis, legend etc for atomic_properties_lda2015 module.
    '''
    source = ColumnDataSource(dflda)
  
    p2 = figure(plot_width=950, plot_height=500,title="Periodic Table",
           x_range=groups, y_range=list(reversed(periods)), toolbar_location=None,tools="hover")
    
    
    if Feature == 'atomic_ea' :
        color_mapper = linear_cmap(field_name = 'atomic_ea', palette = Colorblind[8], 
                                   low = dflda['atomic_ea'].min(), 
                                   high = dflda['atomic_ea'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ea", "@atomic_ea eV"),
        ]
 
        
    if Feature == 'atomic_ip' :
        color_mapper = linear_cmap(field_name = 'atomic_ip', palette = Colorblind[8], 
                                   low = dflda['atomic_ip'].min(), 
                                   high = dflda['atomic_ip'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ip", "@atomic_ip eV"),
        ]
        
    if Feature == 'atomic number' :
        color_mapper = linear_cmap(field_name = 'atomic number', palette = Colorblind[8], 
                                   low = dflda['atomic number'].min(), 
                                   high = dflda['atomic number'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Atomic number", "@{atomic number}"),
        ("Type", "@metal"),
        ]

        
    if Feature == 'atomic mass' :
        color_mapper = linear_cmap(field_name = 'atomic mass', palette = Colorblind[8], 
                                   low = dflda['atomic mass'].min(), 
                                   high = dflda['atomic mass'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Atomic mass", "@{atomic mass} amu"),
        ("Type", "@metal")
        ]

        
    if Feature == 'atomic_homo' :
        color_mapper = linear_cmap(field_name = 'atomic_homo', palette = Colorblind[8], 
                                   low = dflda['atomic_homo'].min(), 
                                   high = dflda['atomic_homo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_homo", "@atomic_homo eV"),
        ]

      
    if Feature == 'atomic_lumo' :
        color_mapper = linear_cmap(field_name = 'atomic_lumo', palette = Colorblind[8], 
                                   low = dflda['atomic_lumo'].min(), 
                                   high = dflda['atomic_lumo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_lumo", "@atomic_lumo eV"),
        ]

    if Feature == 'atomic_r_s' :
        color_mapper = linear_cmap(field_name = 'atomic_r_s', palette = Colorblind[8], 
                                   low = dflda['atomic_r_s'].min(), 
                                   high = dflda['atomic_r_s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_s", "@atomic_r_s pm"),
        ]

    if Feature == 'atomic_r_p' :
        color_mapper = linear_cmap(field_name = 'atomic_r_p', palette = Colorblind[8], 
                                   low = dflda['atomic_r_p'].min(), 
                                   high = dflda['atomic_r_p'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_p", "@atomic_r_p pm"),
        ]

    if Feature == 'atomic_r_d' :
        color_mapper = linear_cmap(field_name = 'atomic_r_d', palette = Colorblind[8], 
                                   low = dflda['atomic_r_d'].min(), 
                                   high = dflda['atomic_r_d'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_d", "@atomic_r_d pm"),
        ]


    p2.rect("group", "period", 0.90, 0.90, source= source, fill_alpha=0.6,color=color_mapper)
    text_props = {"source":source, "text_align": "left", "text_baseline": "middle", "text_color" : "black"}

    x1 = dodge("group", -0.4, range=p2.x_range)

    r1 = p2.text(x=x1, y="period", text="symbol", **text_props)
    r1.glyph.text_font_style="bold"

    r1 = p2.text(x=x1, y=dodge("period", 0.3, range=p2.y_range), text="atomic number", **text_props)
    r1.glyph.text_font_size="8pt"

    r1 = p2.text(x=x1, y=dodge("period", -0.35, range=p2.y_range), text="name", **text_props)
    r1.glyph.text_font_size="5pt"
    #Defines color bar
    color_bar = ColorBar(color_mapper=color_mapper['transform'],
                         formatter = NumeralTickFormatter(format='0.0000'),label_standoff = 13, width=8, location=(0,0))
    # Set color_bar location
    p2.add_layout(color_bar, 'right')
    p2.outline_line_color = None
    p2.grid.grid_line_color = None
    p2.axis.axis_line_color = None
    p2.axis.major_tick_line_color = None
    p2.axis.major_label_standoff = 0
    p2.axis.visible = False
    output_notebook(hide_banner=True)
    show(p2,notebook_handle=True)
    push_notebook()


def ptableplotgenmatmin(Feature):
    '''
    Sets all the heatmap plot properties like text, colour mappers ,tittle, axis, legend etc for atomic_properties_magpie module.
    '''
    source = ColumnDataSource(dfmatm)
  
    p2 = figure(plot_width=950, plot_height=500,title="Periodic Table",
           x_range=groups, y_range=list(reversed(periods)), toolbar_location=None,tools="hover")
    
    if Feature == 'atomic number' :
        color_mapper = linear_cmap(field_name = 'atomic number', palette = Colorblind[8], 
                                   low = dfmatm['atomic number'].min(), 
                                   high = dfmatm['atomic number'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Atomic number", "@{atomic number}"),
        ("Type", "@metal"),
        ]
        
    if Feature == 'atomic mass' :
        color_mapper = linear_cmap(field_name = 'atomic mass', palette = Colorblind[8], 
                                   low = dfmatm['atomic mass'].min(), 
                                   high = dfmatm['atomic mass'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Atomic mass", "@{atomic mass} amu"),
        ("Type", "@metal")
        ]
        
    if Feature == 'atomic_ICSD_vol' :
        color_mapper = linear_cmap(field_name = 'atomic_ICSD_vol', palette = Colorblind[8], 
                                   low = dfmatm['atomic_ICSD_vol'].min(), 
                                   high = dfmatm['atomic_ICSD_vol'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ICSD_vol", "@atomic_ICSD_vol eV"),
        ]
        
    if Feature == 'atomic_cpmass' :
        color_mapper = linear_cmap(field_name = 'atomic_cpmass', palette = Colorblind[8], 
                                   low = dfmatm['atomic_cpmass'].min(), 
                                   high = dfmatm['atomic_cpmass'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_cpmass", "@atomic_cpmass J/g-K"),
        ]
        
    if Feature == 'atomic_cpmolar' :
        color_mapper = linear_cmap(field_name = 'atomic_cpmolar', palette = Colorblind[8], 
                                   low = dfmatm['atomic_cpmolar'].min(), 
                                   high = dfmatm['atomic_cpmolar'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_cpmolar", "@atomic_cpmolar J/mol-K"),
        ("Type", "@metal"),
        ]
        
    if Feature == 'atomic_density' :
        color_mapper = linear_cmap(field_name = 'atomic_density', palette = Colorblind[8], 
                                   low = dfmatm['atomic_density'].min(), 
                                   high = dfmatm['atomic_density'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_density", "@atomic_density g/L"),
        ("Type", "@metal")
        ]
        
    if Feature == 'atomic_ea' :
        color_mapper = linear_cmap(field_name = 'atomic_ea', palette = Colorblind[8], 
                                   low = dfmatm['atomic_ea'].min(), 
                                   high = dfmatm['atomic_ea'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ea", "@atomic_ea kJ/mol"),
        ]
      
    if Feature == 'atomic_en' :
        color_mapper = linear_cmap(field_name = 'atomic_en', palette = Colorblind[8], 
                                   low = dfmatm['atomic_en'].min(), 
                                   high = dfmatm['atomic_en'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_en", "@atomic_en"),
        ]

    if Feature == 'atomic_en_allen' :
        color_mapper = linear_cmap(field_name = 'atomic_en_allen', palette = Colorblind[8], 
                                   low = dfmatm['atomic_en_allen'].min(), 
                                   high = dfmatm['atomic_en_allen'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_en_allen", "@atomic_en_allen"),
        ]

    if Feature == 'atomic_hfu' :
        color_mapper = linear_cmap(field_name = 'atomic_hfu', palette = Colorblind[8], 
                                   low = dfmatm['atomic_hfu'].min(), 
                                   high = dfmatm['atomic_hfu'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_hfu", "@atomic_hfu kJ/mol"),
        ]

    if Feature == 'atomic_hhip' :
        color_mapper = linear_cmap(field_name = 'atomic_hhip', palette = Colorblind[8], 
                                   low = dfmatm['atomic_hhip'].min(), 
                                   high = dfmatm['atomic_hhip'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_hhip", "@atomic_hhip"),
        ]
        
    if Feature == 'atomic_hhir' :
        color_mapper = linear_cmap(field_name = 'atomic_hhir', palette = Colorblind[8], 
                                   low = dfmatm['atomic_hhir'].min(), 
                                   high = dfmatm['atomic_hhir'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_hhir", "@atomic_hhir"),
        ]

    if Feature == 'atomic_hhip' :
        color_mapper = linear_cmap(field_name = 'atomic_hhip', palette = Colorblind[8], 
                                   low = dfmatm['atomic_hhip'].min(), 
                                   high = dfmatm['atomic_hhip'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_hhip", "@atomic_hhip"),
        ]
    
    if Feature == 'atomic_hvap' :
        color_mapper = linear_cmap(field_name = 'atomic_hvap', palette = Colorblind[8], 
                                   low = dfmatm['atomic_hvap'].min(), 
                                   high = dfmatm['atomic_hvap'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_hvap", "@atomic_hvap kJ/mol"),
        ]
    
    if Feature == 'atomic_ie_1' :
        color_mapper = linear_cmap(field_name = 'atomic_ie_1', palette = Colorblind[8], 
                                   low = dfmatm['atomic_ie_1'].min(), 
                                   high = dfmatm['atomic_ie_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ie_1", "@atomic_ie_1 eV"),
        ]
    
    if Feature == 'atomic_ie_2' :
        color_mapper = linear_cmap(field_name = 'atomic_ie_2', palette = Colorblind[8], 
                                   low = dfmatm['atomic_ie_2'].min(), 
                                   high = dfmatm['atomic_ie_2'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ie_2", "@atomic_ie_2 eV"),
        ]
        
    if Feature == 'atomic_ndunf' :
        color_mapper = linear_cmap(field_name = 'atomic_ndunf', palette = Colorblind[8], 
                                   low = dfmatm['atomic_ndunf'].min(), 
                                   high = dfmatm['atomic_ndunf'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ndunf", "@atomic_ndunf"),
        ]
    
    if Feature == 'atomic_ndval' :
        color_mapper = linear_cmap(field_name = 'atomic_ndval', palette = Colorblind[8], 
                                   low = dfmatm['atomic_ndval'].min(), 
                                   high = dfmatm['atomic_ndval'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ndval", "@atomic_ndval"),
        ]
    
    if Feature == 'atomic_nfunf' :
        color_mapper = linear_cmap(field_name = 'atomic_nfunf', palette = Colorblind[8], 
                                   low = dfmatm['atomic_nfunf'].min(), 
                                   high = dfmatm['atomic_nfunf'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_nfunf", "@atomic_nfunf"),
        ]
    
    if Feature == 'atomic_nfval' :
        color_mapper = linear_cmap(field_name = 'atomic_nfval', palette = Colorblind[8], 
                                   low = dfmatm['atomic_nfval'].min(), 
                                   high = dfmatm['atomic_nfval'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_nfval", "@atomic_nfval"),
        ]
        
    if Feature == 'atomic_npunf' :
        color_mapper = linear_cmap(field_name = 'atomic_npunf', palette = Colorblind[8], 
                                   low = dfmatm['atomic_npunf'].min(), 
                                   high = dfmatm['atomic_npunf'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_npunf", "@atomic_npunf"),
        ]

    if Feature == 'atomic_npval' :
        color_mapper = linear_cmap(field_name = 'atomic_npval', palette = Colorblind[8], 
                                   low = dfmatm['atomic_npval'].min(), 
                                   high = dfmatm['atomic_npval'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_npval", "@atomic_npval"),
        ]
    
    if Feature == 'atomic_nsunf' :
        color_mapper = linear_cmap(field_name = 'atomic_nsunf', palette = Colorblind[8], 
                                   low = dfmatm['atomic_nsunf'].min(), 
                                   high = dfmatm['atomic_nsunf'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_nsunf", "@atomic_nsunf"),
        ]
    
    if Feature == 'atomic_nsval' :
        color_mapper = linear_cmap(field_name = 'atomic_nsval', palette = Colorblind[8], 
                                   low = dfmatm['atomic_nsval'].min(), 
                                   high = dfmatm['atomic_nsval'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_nsval", "@atomic_nsval"),
        ]
    
    if Feature == 'atomic_number' :
        color_mapper = linear_cmap(field_name = 'atomic_number', palette = Colorblind[8], 
                                   low = dfmatm['atomic_number'].min(), 
                                   high = dfmatm['atomic_number'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_number", "@atomic_number"),
        ]
    
    if Feature == 'atomic_nunf' :
        color_mapper = linear_cmap(field_name = 'atomic_nunf', palette = Colorblind[8], 
                                   low = dfmatm['atomic_nunf'].min(), 
                                   high = dfmatm['atomic_nunf'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_nunf", "@atomic_nunf"),
        ]
    
    if Feature == 'atomic_nval' :
        color_mapper = linear_cmap(field_name = 'atomic_nval', palette = Colorblind[8], 
                                   low = dfmatm['atomic_nval'].min(), 
                                   high = dfmatm['atomic_nval'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_nval", "@atomic_nval"),
        ]
    
    if Feature == 'atomic_phi' :
        color_mapper = linear_cmap(field_name = 'atomic_phi', palette = Colorblind[8], 
                                   low = dfmatm['atomic_phi'].min(), 
                                   high = dfmatm['atomic_phi'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_phi", "@atomic_phi eV"),
        ]

    if Feature == 'atomic_pol' :
        color_mapper = linear_cmap(field_name = 'atomic_pol', palette = Colorblind[8], 
                                   low = dfmatm['atomic_pol'].min(), 
                                   high = dfmatm['atomic_pol'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_pol", "@atomic_pol 10^-24 cm^3"),
        ]
     
    if Feature == 'atomic_pp_r_d' :
        color_mapper = linear_cmap(field_name = 'atomic_pp_r_d', palette = Colorblind[8], 
                                   low = dfmatm['atomic_pp_r_d'].min(), 
                                   high = dfmatm['atomic_pp_r_d'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_pp_r_d", "@atomic_pp_r_d a.u"),
        ]
    
    if Feature == 'atomic_pp_r_p' :
        color_mapper = linear_cmap(field_name = 'atomic_pp_r_p', palette = Colorblind[8], 
                                   low = dfmatm['atomic_pp_r_p'].min(), 
                                   high = dfmatm['atomic_pp_r_p'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_pp_r_p", "@atomic_pp_r_p a.u"),
        ]
    
    if Feature == 'atomic_pp_r_s' :
        color_mapper = linear_cmap(field_name = 'atomic_pp_r_s', palette = Colorblind[8], 
                                   low = dfmatm['atomic_pp_r_s'].min(), 
                                   high = dfmatm['atomic_pp_r_s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_pp_r_s", "@atomic_pp_r_s a.u"),
        ]
    
    if Feature == 'atomic_pp_r_pi' :
        color_mapper = linear_cmap(field_name = 'atomic_pp_r_pi', palette = Colorblind[8], 
                                   low = dfmatm['atomic_pp_r_pi'].min(), 
                                   high = dfmatm['atomic_pp_r_pi'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_pp_r_pi", "@atomic_pp_r_pi a.u"),
        ]
    
    if Feature == 'atomic_pp_r_sig' :
        color_mapper = linear_cmap(field_name = 'atomic_pp_r_sig', palette = Colorblind[8], 
                                   low = dfmatm['atomic_pp_r_sig'].min(), 
                                   high = dfmatm['atomic_pp_r_sig'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_pp_r_sig", "@atomic_pp_r_sig a.u"),
        ]
    
    if Feature == 'atomic_radius' :
        color_mapper = linear_cmap(field_name = 'atomic_radius', palette = Colorblind[8], 
                                   low = dfmatm['atomic_radius'].min(), 
                                   high = dfmatm['atomic_radius'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_radius", "@atomic_radius Angs"),
        ]
    
    if Feature == 'atomic_sgn' :
        color_mapper = linear_cmap(field_name = 'atomic_sgn', palette = Colorblind[8], 
                                   low = dfmatm['atomic_sgn'].min(), 
                                   high = dfmatm['atomic_sgn'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_sgn", "@atomic_sgn"),
        ]
    
    if Feature == 'atomic_vdw_radius' :
        color_mapper = linear_cmap(field_name = 'atomic_vdw_radius', palette = Colorblind[8], 
                                   low = dfmatm['atomic_vdw_radius'].min(), 
                                   high = dfmatm['atomic_vdw_radius'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_vdw_radius", "@atomic_vdw_radius Angs"),
        ]
    
    if Feature == 'atomic_volume' :
        color_mapper = linear_cmap(field_name = 'atomic_volume', palette = Colorblind[8], 
                                   low = dfmatm['atomic_volume'].min(), 
                                   high = dfmatm['atomic_volume'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_volume", "@atomic_volume A^3/atom"),
        ]
    
    if Feature == 'atomic_weight' :
        color_mapper = linear_cmap(field_name = 'atomic_weight', palette = Colorblind[8], 
                                   low = dfmatm['atomic_weight'].min(), 
                                   high = dfmatm['atomic_weight'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_weight", "@atomic_weight"),
        ]
    
    if Feature == 'atomic_ws3' :
        color_mapper = linear_cmap(field_name = 'atomic_ws3', palette = Colorblind[8], 
                                   low = dfmatm['atomic_ws3'].min(), 
                                   high = dfmatm['atomic_ws3'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ws3", "@atomic_ws3"),
        ]
        
    if Feature == 'boiling_temp' :
        color_mapper = linear_cmap(field_name = 'boiling_temp', palette = Colorblind[8], 
                                   low = dfmatm['boiling_temp'].min(), 
                                   high = dfmatm['boiling_temp'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("boiling_temp", "@boiling_temp K"),
        ]
    
    if Feature == 'bulk_modulus' :
        color_mapper = linear_cmap(field_name = 'bulk_modulus', palette = Colorblind[8], 
                                   low = dfmatm['bulk_modulus'].min(), 
                                   high = dfmatm['bulk_modulus'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("bulk_modulus", "@bulk_modulus GPa"),
        ]
    
    if Feature == 'covalent_radius' :
        color_mapper = linear_cmap(field_name = 'covalent_radius', palette = Colorblind[8], 
                                   low = dfmatm['covalent_radius'].min(), 
                                   high = dfmatm['covalent_radius'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("covalent_radius", "@covalent_radius pm"),
        ]
    
    if Feature == 'gs_bandgap' :
        color_mapper = linear_cmap(field_name = 'gs_bandgap', palette = Colorblind[8], 
                                   low = dfmatm['gs_bandgap'].min(), 
                                   high = dfmatm['gs_bandgap'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("gs_bandgap", "@gs_bandgap eV"),
        ]
    
    if Feature == 'gs_bcclatparam' :
        color_mapper = linear_cmap(field_name = 'gs_bcclatparam', palette = Colorblind[8], 
                                   low = dfmatm['gs_bcclatparam'].min(), 
                                   high = dfmatm['gs_bcclatparam'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("gs_bcclatparam", "@gs_bcclatparam Angs"),
        ]
    
    if Feature == 'gs_energy_pa' :
        color_mapper = linear_cmap(field_name = 'gs_energy_pa', palette = Colorblind[8], 
                                   low = dfmatm['gs_energy_pa'].min(), 
                                   high = dfmatm['gs_energy_pa'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("gs_energy_pa", "@gs_energy_pa eV"),
        ]
    
    if Feature == 'gs_fcclatparam' :
        color_mapper = linear_cmap(field_name = 'gs_fcclatparam', palette = Colorblind[8], 
                                   low = dfmatm['gs_fcclatparam'].min(), 
                                   high = dfmatm['gs_fcclatparam'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("gs_fcclatparam", "@gs_fcclatparam Angs"),
        ]
    
    if Feature == 'gs_mag_mom' :
        color_mapper = linear_cmap(field_name = 'gs_mag_mom', palette = Colorblind[8], 
                                   low = dfmatm['gs_mag_mom'].min(), 
                                   high = dfmatm['gs_mag_mom'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("gs_mag_mom", "@gs_mag_mom"),
        ]
    
    if Feature == 'gs_volume_pa' :
        color_mapper = linear_cmap(field_name = 'gs_volume_pa', palette = Colorblind[8], 
                                   low = dfmatm['gs_volume_pa'].min(), 
                                   high = dfmatm['gs_volume_pa'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("gs_volume_pa", "@gs_volume_pa Angs^3"),
        ]
    
    if Feature == 'melting_point' :
        color_mapper = linear_cmap(field_name = 'melting_point', palette = Colorblind[8], 
                                   low = dfmatm['melting_point'].min(), 
                                   high = dfmatm['melting_point'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("melting_point", "@melting_point K"),
        ]
        
    if Feature == 'mendeleev_no' :
        color_mapper = linear_cmap(field_name = 'mendeleev_no', palette = Colorblind[8], 
                                   low = dfmatm['mendeleev_no'].min(), 
                                   high = dfmatm['mendeleev_no'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("mendeleev_no", "@mendeleev_no"),
        ]
    
    if Feature == 'micracle_radius' :
        color_mapper = linear_cmap(field_name = 'micracle_radius', palette = Colorblind[8], 
                                   low = dfmatm['micracle_radius'].min(), 
                                   high = dfmatm['micracle_radius'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("micracle_radius", "@micracle_radius pm"),
        ]
    
    if Feature == 'molar_volume' :
        color_mapper = linear_cmap(field_name = 'molar_volume', palette = Colorblind[8], 
                                   low = dfmatm['molar_volume'].min(), 
                                   high = dfmatm['molar_volume'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("molar_volume", "@molar_volume cm^3"),
        ]
    
    if Feature == 'shear_mod' :
        color_mapper = linear_cmap(field_name = 'shear_mod', palette = Colorblind[8], 
                                   low = dfmatm['shear_mod'].min(), 
                                   high = dfmatm['shear_mod'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("shear_mod", "@shear_mod GPa"),
        ]
    
    if Feature == 'thermal_cond' :
        color_mapper = linear_cmap(field_name = 'thermal_cond', palette = Colorblind[8], 
                                   low = dfmatm['thermal_cond'].min(), 
                                   high = dfmatm['thermal_cond'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("thermal_cond", "@thermal_cond W/m-K"),
        ]
    
    if Feature == 'thermal_cond_log' :
        color_mapper = linear_cmap(field_name = 'thermal_cond_log', palette = Colorblind[8], 
                                   low = dfmatm['thermal_cond_log'].min(), 
                                   high = dfmatm['thermal_cond_log'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("thermal_cond_log", "@thermal_cond_log"),
        ]
    
    
    p2.rect("group", "period", 0.90, 0.90, source= source, fill_alpha=0.6,color=color_mapper)
    text_props = {"source":source, "text_align": "left", "text_baseline": "middle", "text_color" : "black"}

    x1 = dodge("group", -0.4, range=p2.x_range)

    r1 = p2.text(x=x1, y="period", text="symbol", **text_props)
    r1.glyph.text_font_style="bold"

    r1 = p2.text(x=x1, y=dodge("period", 0.3, range=p2.y_range), text="atomic number", **text_props)
    r1.glyph.text_font_size="8pt"

    r1 = p2.text(x=x1, y=dodge("period", -0.35, range=p2.y_range), text="name", **text_props)
    r1.glyph.text_font_size="5pt"
    #Defines color bar
    color_bar = ColorBar(color_mapper=color_mapper['transform'],
                         formatter = NumeralTickFormatter(format='0.0000'),label_standoff = 13, width=8, location=(0,0))
    # Set color_bar location
    p2.add_layout(color_bar, 'right')
    p2.outline_line_color = None
    p2.grid.grid_line_color = None
    p2.axis.axis_line_color = None
    p2.axis.major_tick_line_color = None
    p2.axis.major_label_standoff = 0
    p2.axis.visible = False
    output_notebook(hide_banner=True)
    show(p2,notebook_handle=True)
    push_notebook()
    
def ptableplotgenpymat(Feature):
    '''
    Sets all the heatmap plot properties like text, colour mappers ,tittle, axis, legend etc for atomic_properties_pymat module.
    '''
    source = ColumnDataSource(dfpymat)
  
    p2 = figure(plot_width=950, plot_height=500,title="Periodic Table",
           x_range=groups, y_range=list(reversed(periods)), toolbar_location=None,tools="hover")
    
    if Feature == 'atomic number' :
        color_mapper = linear_cmap(field_name = 'atomic number', palette = Colorblind[8], 
                                   low = dfpymat['atomic number'].min(), 
                                   high = dfpymat['atomic number'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Atomic number", "@{atomic number}"),
        ("Type", "@metal"),
        ]

    if Feature == 'atomic mass' :
        color_mapper = linear_cmap(field_name = 'atomic mass', palette = Colorblind[8], 
                                   low = dfpymat['atomic mass'].min(), 
                                   high = dfpymat['atomic mass'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Atomic mass", "@{atomic mass} amu"),
        ("Type", "@metal")
        ]

    if Feature == 'atomic_radius' :
        color_mapper = linear_cmap(field_name = 'atomic_radius', palette = Colorblind[8], 
                                   low = dfpymat['atomic_radius'].min(), 
                                   high = dfpymat['atomic_radius'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ICSD_vol", "@atomic_radius Ang"),
        ]

    if Feature == 'atomic_radius_calculated' :
        color_mapper = linear_cmap(field_name = 'atomic_radius_calculated', palette = Colorblind[8], 
                                   low = dfpymat['atomic_radius_calculated'].min(), 
                                   high = dfpymat['atomic_radius_calculated'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_radius_calculated", "@atomic_radius_calculated Angs"),
        ]

    if Feature == 'boiling_point' :
        color_mapper = linear_cmap(field_name = 'boiling_point', palette = Colorblind[8], 
                                   low = dfpymat['boiling_point'].min(), 
                                   high = dfpymat['boiling_point'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("boiling_point", "@boiling_point K"),
        ("Type", "@metal"),
        ]

    if Feature == 'brinell_hardness' :
        color_mapper = linear_cmap(field_name = 'brinell_hardness', palette = Colorblind[8], 
                                   low = dfpymat['brinell_hardness'].min(), 
                                   high = dfpymat['brinell_hardness'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("brinell_hardness", "@brinell_hardness [MN]/[m^2]"),
        ("Type", "@metal"),
        ]

    if Feature == 'bulk_modulus' :
        color_mapper = linear_cmap(field_name = 'bulk_modulus', palette = Colorblind[8], 
                                   low = dfpymat['bulk_modulus'].min(), 
                                   high = dfpymat['bulk_modulus'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("bulk_modulus", "@bulk_modulus GPa"),
        ("Type", "@metal"),
        ]

    if Feature == 'coefficient_olte' :
        color_mapper = linear_cmap(field_name = 'coefficient_olte', palette = Colorblind[8], 
                                   low = dfpymat['coefficient_olte'].min(), 
                                   high = dfpymat['coefficient_olte'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("coefficient_olte", "@coefficient_olte [x10^-6]/[K]"),
        ("Type", "@metal"),
        ]

    if Feature == 'critical_temperature' :
        color_mapper = linear_cmap(field_name = 'critical_temperature', palette = Colorblind[8], 
                                   low = dfpymat['critical_temperature'].min(), 
                                   high = dfpymat['critical_temperature'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("critical_temperature", "@critical_temperature K"),
        ("Type", "@metal"),
        ]

    if Feature == 'density_of_solid' :
        color_mapper = linear_cmap(field_name = 'density_of_solid', palette = Colorblind[8], 
                                   low = dfpymat['density_of_solid'].min(), 
                                   high = dfpymat['density_of_solid'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("density_of_solid", "@density_of_solid [Kg]/[m^3]"),
        ("Type", "@metal"),
        ]

    if Feature == 'electrical_resistivity' :
        color_mapper = linear_cmap(field_name = 'electrical_resistivity', palette = Colorblind[8], 
                                   low = dfpymat['electrical_resistivity'].min(), 
                                   high = dfpymat['electrical_resistivity'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("electrical_resistivity", "@electrical_resistivity [10^-8] Omega.m"),
        ("Type", "@metal"),
        ]

    if Feature == 'liquid_range' :
        color_mapper = linear_cmap(field_name = 'liquid_range', palette = Colorblind[8], 
                                   low = dfpymat['liquid_range'].min(), 
                                   high = dfpymat['liquid_range'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("liquid_range", "@liquid_range K"),
        ("Type", "@metal"),
        ]

    if Feature == 'melting_point' :
        color_mapper = linear_cmap(field_name = 'melting_point', palette = Colorblind[8], 
                                   low = dfpymat['melting_point'].min(), 
                                   high = dfpymat['melting_point'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("melting_point", "@melting_point K"),
        ("Type", "@metal"),
        ]

    if Feature == 'mineral_hardness' :
        color_mapper = linear_cmap(field_name = 'mineral_hardness', palette = Colorblind[8], 
                                   low = dfpymat['mineral_hardness'].min(), 
                                   high = dfpymat['mineral_hardness'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("mineral_hardness", "@mineral_hardness "),
        ("Type", "@metal"),
        ]

    if Feature == 'molar_volume' :
        color_mapper = linear_cmap(field_name = 'molar_volume', palette = Colorblind[8], 
                                   low = dfpymat['molar_volume'].min(), 
                                   high = dfpymat['molar_volume'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("molar_volume", "@molar_volume cm^3"),
        ("Type", "@metal"),
        ]

    if Feature == 'poisson_ratio' :
        color_mapper = linear_cmap(field_name = 'poisson_ratio', palette = Colorblind[8], 
                                   low = dfpymat['poisson_ratio'].min(), 
                                   high = dfpymat['poisson_ratio'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("poisson_ratio", "@poisson_ratio "),
        ("Type", "@metal"),
        ]

    if Feature == 'atomic_refelctivity' :
        color_mapper = linear_cmap(field_name = 'atomic_refelctivity', palette = Colorblind[8], 
                                   low = dfpymat['atomic_refelctivity'].min(), 
                                   high = dfpymat['atomic_refelctivity'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_refelctivity", "@atomic_refelctivity %"),
        ("Type", "@metal"),
        ]

    if Feature == 'rigidity_modulus' :
        color_mapper = linear_cmap(field_name = 'rigidity_modulus', palette = Colorblind[8], 
                                   low = dfpymat['rigidity_modulus'].min(), 
                                   high = dfpymat['rigidity_modulus'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("rigidity_modulus", "@rigidity_modulus GPa"),
        ("Type", "@metal"),
        ]

    if Feature == 'thermal_cond' :
        color_mapper = linear_cmap(field_name = 'thermal_cond', palette = Colorblind[8], 
                                   low = dfpymat['thermal_cond'].min(), 
                                   high = dfpymat['thermal_cond'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("thermal_cond", "@thermal_cond W/[mK"),
        ("Type", "@metal"),
        ]

    if Feature == 'van_der_waals_rad' :
        color_mapper = linear_cmap(field_name = 'van_der_waals_rad', palette = Colorblind[8], 
                                   low = dfpymat['van_der_waals_rad'].min(), 
                                   high = dfpymat['van_der_waals_rad'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("van_der_waals_rad", "@van_der_waals_rad"),
        ("Type", "@metal"),
        ]

    if Feature == 'velocity_of_sound' :
        color_mapper = linear_cmap(field_name = 'velocity_of_sound', palette = Colorblind[8], 
                                   low = dfpymat['velocity_of_sound'].min(), 
                                   high = dfpymat['velocity_of_sound'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("velocity_of_sound", "@velocity_of_sound m/s"),
        ("Type", "@metal"),
        ]

    if Feature == 'vickers_hardness' :
        color_mapper = linear_cmap(field_name = 'vickers_hardness', palette = Colorblind[8], 
                                   low = dfpymat['vickers_hardness'].min(), 
                                   high = dfpymat['vickers_hardness'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("vickers_hardness", "@vickers_hardness MN/[m^-2]"),
        ("Type", "@metal"),
        ]

    if Feature == 'x' :
        color_mapper = linear_cmap(field_name = 'x', palette = Colorblind[8], 
                                   low = dfpymat['x'].min(), 
                                   high = dfpymat['x'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("x", "@x"),
        ("Type", "@metal"),
        ]

    if Feature == 'youngs_modulus' :
        color_mapper = linear_cmap(field_name = 'youngs_modulus', palette = Colorblind[8], 
                                   low = dfpymat['youngs_modulus'].min(), 
                                   high = dfpymat['youngs_modulus'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("youngs_modulus", "@youngs_modulus GPa"),
        ("Type", "@metal"),
        ]
        
    if Feature == 'metallic_radius' :
        color_mapper = linear_cmap(field_name = 'metallic_radius', palette = Colorblind[8], 
                                   low = dfpymat['metallic_radius'].min(), 
                                   high = dfpymat['metallic_radius'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("metallic_radius", "@metallic_radius Angs"),
        ("Type", "@metal"),
        ]
        
    if Feature == 'iupac_ordering' :
        color_mapper = linear_cmap(field_name = 'iupac_ordering', palette = Colorblind[8], 
                                   low = dfpymat['iupac_ordering'].min(), 
                                   high = dfpymat['iupac_ordering'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("iupac_ordering", "@iupac_ordering"),
        ("Type", "@metal"),
        ]
        
    if Feature == 'supercond_temp' :
        color_mapper = linear_cmap(field_name = 'supercond_temp', palette = Colorblind[8], 
                                   low = dfpymat['supercond_temp'].min(), 
                                   high = dfpymat['supercond_temp'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("supercond_temp", "@supercond_temp K"),
        ("Type", "@metal"),
        ]
     
    if Feature == 'min_oxd_state' :
        color_mapper = linear_cmap(field_name = 'min_oxd_state', palette = Colorblind[8], 
                                   low = dfpymat['min_oxd_state'].min(), 
                                   high = dfpymat['min_oxd_state'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("min_oxd_state", "@min_oxd_state"),
        ("Type", "@metal"),
        ]
     
    if Feature == 'max_oxd_state' :
        color_mapper = linear_cmap(field_name = 'max_oxd_state', palette = Colorblind[8], 
                                   low = dfpymat['max_oxd_state'].min(), 
                                   high = dfpymat['max_oxd_state'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("max_oxd_state", "@max_oxd_state"),
        ("Type", "@metal"),
        ]
    
    if Feature == 'atomic_orbitals_1s' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_1s', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_1s'].min(), 
                                   high = dfpymat['atomic_orbitals_1s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_1s", "@atomic_orbitals_1s eV"),
        ("Type", "@metal"),
        ]
       
    
    if Feature == 'atomic_orbitals_2s' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_2s', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_2s'].min(), 
                                   high = dfpymat['atomic_orbitals_2s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_2s", "@atomic_orbitals_2s eV"),
        ("Type", "@metal"),
        ]
        
    
    if Feature == 'atomic_orbitals_2p' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_2p', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_2p'].min(), 
                                   high = dfpymat['atomic_orbitals_2p'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_2p", "@atomic_orbitals_2p eV"),
        ("Type", "@metal"),
        ]
   
        
    if Feature == 'atomic_orbitals_3s' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_3s', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_3s'].min(), 
                                   high = dfpymat['atomic_orbitals_3s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_3s", "@atomic_orbitals_3s eV"),
        ("Type", "@metal"),
        ]

    
    if Feature == 'atomic_orbitals_3p' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_3p', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_3p'].min(), 
                                   high = dfpymat['atomic_orbitals_3p'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_3p", "@atomic_orbitals_3p eV"),
        ("Type", "@metal"),
        ]

    
    if Feature == 'atomic_orbitals_3d' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_3d', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_3d'].min(), 
                                   high = dfpymat['atomic_orbitals_3d'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_3d", "@atomic_orbitals_3d eV"),
        ("Type", "@metal"),
        ]

    
    if Feature == 'atomic_orbitals_4s' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_4s', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_4s'].min(), 
                                   high = dfpymat['atomic_orbitals_4s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_4s", "@atomic_orbitals_4s eV"),
        ("Type", "@metal"),
        ]

    
    if Feature == 'atomic_orbitals_4p' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_4p', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_4p'].min(), 
                                   high = dfpymat['atomic_orbitals_4p'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_4p", "@atomic_orbitals_4p eV"),
        ("Type", "@metal"),
        ]

    
    if Feature == 'atomic_orbitals_4d' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_4d', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_4d'].min(), 
                                   high = dfpymat['atomic_orbitals_4d'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_4d", "@atomic_orbitals_4d eV"),
        ("Type", "@metal"),
        ]

    
    if Feature == 'atomic_orbitals_5s' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_5s', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_5s'].min(), 
                                   high = dfpymat['atomic_orbitals_5s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_5s", "@atomic_orbitals_5s eV"),
        ("Type", "@metal"),
        ]

    
    if Feature == 'atomic_orbitals_5p' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_5p', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_5p'].min(), 
                                   high = dfpymat['atomic_orbitals_5p'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_5p", "@atomic_orbitals_5p eV"),
        ("Type", "@metal"),
        ]

    
    if Feature == 'atomic_orbitals_5d' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_5d', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_5d'].min(), 
                                   high = dfpymat['atomic_orbitals_5d'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_5d", "@atomic_orbitals_5d eV"),
        ("Type", "@metal"),
        ]

    
    if Feature == 'atomic_orbitals_6s' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_6s', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_6s'].min(), 
                                   high = dfpymat['atomic_orbitals_6s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_6s", "@atomic_orbitals_6s eV"),
        ("Type", "@metal"),
        ]

    
    if Feature == 'atomic_orbitals_6p' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_6p', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_6p'].min(), 
                                   high = dfpymat['atomic_orbitals_6p'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_6p", "@atomic_orbitals_6p eV"),
        ("Type", "@metal"),
        ]

    
    if Feature == 'atomic_orbitals_6d' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_6d', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_6d'].min(), 
                                   high = dfpymat['atomic_orbitals_6d'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_6d", "@atomic_orbitals_6d eV"),
        ("Type", "@metal"),
        ]

    
    if Feature == 'atomic_orbitals_7s' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_7s', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_7s'].min(), 
                                   high = dfpymat['atomic_orbitals_7s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_7s", "@atomic_orbitals_7s eV"),
        ("Type", "@metal"),
        ]

    
    if Feature == 'atomic_orbitals_4f' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_4f', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_4f'].min(), 
                                   high = dfpymat['atomic_orbitals_4f'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_4f", "@atomic_orbitals_4f eV"),
        ("Type", "@metal"),
        ]

    
    if Feature == 'atomic_orbitals_5f' :
        color_mapper = linear_cmap(field_name = 'atomic_orbitals_5f', palette = Colorblind[8], 
                                   low = dfpymat['atomic_orbitals_5f'].min(), 
                                   high = dfpymat['atomic_orbitals_5f'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("atomic_orbitals_5f", "@atomic_orbitals_5f eV"),
        ("Type", "@metal"),
        ]



    p2.rect("group", "period", 0.90, 0.90, source= source, fill_alpha=0.6,color=color_mapper)
    text_props = {"source":source, "text_align": "left", "text_baseline": "middle", "text_color" : "black"}

    x1 = dodge("group", -0.4, range=p2.x_range)

    r1 = p2.text(x=x1, y="period", text="symbol", **text_props)
    r1.glyph.text_font_style="bold"

    r1 = p2.text(x=x1, y=dodge("period", 0.3, range=p2.y_range), text="atomic number", **text_props)
    r1.glyph.text_font_size="8pt"

    r1 = p2.text(x=x1, y=dodge("period", -0.35, range=p2.y_range), text="name", **text_props)
    r1.glyph.text_font_size="5pt"
    #Defines color bar
    color_bar = ColorBar(color_mapper=color_mapper['transform'],
                         formatter = NumeralTickFormatter(format='0.0000'),label_standoff = 13, width=8, location=(0,0))
    # Set color_bar location
    p2.add_layout(color_bar, 'right')
    p2.outline_line_color = None
    p2.grid.grid_line_color = None
    p2.axis.axis_line_color = None
    p2.axis.major_tick_line_color = None
    p2.axis.major_label_standoff = 0
    p2.axis.visible = False
    output_notebook(hide_banner=True)
    show(p2,notebook_handle=True)
    push_notebook()

def ptableplotgenwebele(Feature):
    '''
    Sets all the heatmap plot properties like text, colour mappers ,tittle, axis, legend etc for atomic_properties_webele module.
    '''
    source = ColumnDataSource(dfwebele)
  
    p2 = figure(plot_width=950, plot_height=500,title="Periodic Table",
           x_range=groups, y_range=list(reversed(periods)), toolbar_location=None,tools="hover")
    
    
    if Feature == 'electron_aff' :
        color_mapper = linear_cmap(field_name = 'electron_aff', palette = Colorblind[8], 
                                   low = dfwebele['electron_aff'].min(), 
                                   high = dfwebele['electron_aff'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("electron_aff", "@electron_aff kJ/mol"),
        ]
        
        
    if Feature == 'atomic mass' :
        color_mapper = linear_cmap(field_name = 'atomic mass', palette = Colorblind[8], 
                                   low = dfwebele['atomic mass'].min(), 
                                   high = dfwebele['atomic mass'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic mass", "@atomic mass amu"),
        ]
        
        
    if Feature == 'atomic number' :
        color_mapper = linear_cmap(field_name = 'atomic number', palette = Colorblind[8], 
                                   low = dfwebele['atomic number'].min(), 
                                   high = dfwebele['atomic number'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Atomic number", "@{atomic number}"),
        ("Type", "@metal"),
        ]
        
        
    if Feature == 'density_of_solid' :
        color_mapper = linear_cmap(field_name = 'density_of_solid', palette = Colorblind[8], 
                                   low = dfwebele['density_of_solid'].min(), 
                                   high = dfwebele['density_of_solid'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("density_of_solid", "@density_of_solid (kg/m3)"),
        ]
        
    
    if Feature == 'molar_vol' :
        color_mapper = linear_cmap(field_name = 'molar_vol', palette = Colorblind[8], 
                                   low = dfwebele['molar_vol'].min(), 
                                   high = dfwebele['molar_vol'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("molar_vol", "@molar_vol  cm3"),
        ]
       
        
    if Feature == 'youngs_mod' :
        color_mapper = linear_cmap(field_name = 'youngs_mod', palette = Colorblind[8], 
                                   low = dfwebele['youngs_mod'].min(), 
                                   high = dfwebele['youngs_mod'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("youngs_mod", "@youngs_mod GPa"),
        ]
       
        
    if Feature == 'rigidity_mod' :
        color_mapper = linear_cmap(field_name = 'rigidity_mod', palette = Colorblind[8], 
                                   low = dfwebele['rigidity_mod'].min(), 
                                   high = dfwebele['rigidity_mod'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("rigidity_mod", "@rigidity_mod GPa"),
        ]
   
        
    if Feature == 'bulk_mod' :
        color_mapper = linear_cmap(field_name = 'bulk_mod', palette = Colorblind[8], 
                                   low = dfwebele['bulk_mod'].min(), 
                                   high = dfwebele['bulk_mod'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("bulk_mod", "@bulk_mod GPa"),
        ]
 
        
    if Feature == 'poissons_ratio' :
        color_mapper = linear_cmap(field_name = 'poissons_ratio', palette = Colorblind[8], 
                                   low = dfwebele['poissons_ratio'].min(), 
                                   high = dfwebele['poissons_ratio'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("poissons_ratio", "@poissons_ratio"),
        ]
     
        
    if Feature == 'mineral_hardness' :
        color_mapper = linear_cmap(field_name = 'mineral_hardness', palette = Colorblind[8], 
                                   low = dfwebele['mineral_hardness'].min(), 
                                   high = dfwebele['mineral_hardness'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("mineral_hardness", "@mineral_hardness"),
        ]
   
    
        
    if Feature == 'brinell_hardness' :
        color_mapper = linear_cmap(field_name = 'brinell_hardness', palette = Colorblind[8], 
                                   low = dfwebele['brinell_hardness'].min(), 
                                   high = dfwebele['brinell_hardness'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("brinell_hardness", "@brinell_hardness"),
        ]

        
    if Feature == 'vickers_hardness' :
        color_mapper = linear_cmap(field_name = 'vickers_hardness', palette = Colorblind[8], 
                                   low = dfwebele['vickers_hardness'].min(), 
                                   high = dfwebele['vickers_hardness'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("vickers_hardness", "@vickers_hardness"),
        ]

        
    if Feature == 'electrical_resistivity' :
        color_mapper = linear_cmap(field_name = 'electrical_resistivity', palette = Colorblind[8], 
                                   low = dfwebele['electrical_resistivity'].min(), 
                                   high = dfwebele['electrical_resistivity'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("electrical_resistivity", "@electrical_resistivity × 10‑8 Ωm"),
        ]

        
    if Feature == 'thermal_conductivity' :
        color_mapper = linear_cmap(field_name = 'thermal_conductivity', palette = Colorblind[8], 
                                   low = dfwebele['thermal_conductivity'].min(), 
                                   high = dfwebele['thermal_conductivity'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("thermal_conductivity", "@thermal_conductivity W/mK"),
        ]

        
    if Feature == 'coeff_of_lte' :
        color_mapper = linear_cmap(field_name = 'coeff_of_lte', palette = Colorblind[8], 
                                   low = dfwebele['coeff_of_lte'].min(), 
                                   high = dfwebele['coeff_of_lte'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("coeff_of_lte", "@coeff_of_lte × 10‑6 /K"),
        ]

        
    if Feature == 'reflectivity' :
        color_mapper = linear_cmap(field_name = 'reflectivity', palette = Colorblind[8], 
                                   low = dfwebele['reflectivity'].min(), 
                                   high = dfwebele['reflectivity'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("reflectivity", "@reflectivity %"),
        ]

        
    if Feature == 'velocity_of_sound' :
        color_mapper = linear_cmap(field_name = 'velocity_of_sound', palette = Colorblind[8], 
                                   low = dfwebele['velocity_of_sound'].min(), 
                                   high = dfwebele['velocity_of_sound'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("velocity_of_sound", "@velocity_of_sound m/s"),
        ]

        
    if Feature == 'eff_nuc_char_2s' :
        color_mapper = linear_cmap(field_name = 'eff_nuc_char_2s', palette = Colorblind[8], 
                                   low = dfwebele['eff_nuc_char_2s'].min(), 
                                   high = dfwebele['eff_nuc_char_2s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("eff_nuc_char_2s", "@eff_nuc_char_2s"),
        ]

        
    if Feature == 'eff_nuc_char_3s' :
        color_mapper = linear_cmap(field_name = 'eff_nuc_char_3s', palette = Colorblind[8], 
                                   low = dfwebele['eff_nuc_char_3s'].min(), 
                                   high = dfwebele['eff_nuc_char_3s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("eff_nuc_char_3s", "@eff_nuc_char_3s"),
        ]

        
    if Feature == 'eff_nuc_char_3p' :
        color_mapper = linear_cmap(field_name = 'eff_nuc_char_3p', palette = Colorblind[8], 
                                   low = dfwebele['eff_nuc_char_3p'].min(), 
                                   high = dfwebele['eff_nuc_char_3p'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("eff_nuc_char_3p", "@eff_nuc_char_3p"),
        ]

        
    if Feature == 'eff_nuc_char_4s' :
        color_mapper = linear_cmap(field_name = 'eff_nuc_char_4s', palette = Colorblind[8], 
                                   low = dfwebele['eff_nuc_char_4s'].min(), 
                                   high = dfwebele['eff_nuc_char_4s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("eff_nuc_char_4s", "@eff_nuc_char_4s"),
        ]

        
    if Feature == 'eff_nuc_char_4p' :
        color_mapper = linear_cmap(field_name = 'eff_nuc_char_4p', palette = Colorblind[8], 
                                   low = dfwebele['eff_nuc_char_4p'].min(), 
                                   high = dfwebele['eff_nuc_char_4p'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("eff_nuc_char_4p", "@eff_nuc_char_4p"),
        ]

        
    if Feature == 'eff_nuc_char_4d' :
        color_mapper = linear_cmap(field_name = 'eff_nuc_char_4d', palette = Colorblind[8], 
                                   low = dfwebele['eff_nuc_char_4d'].min(), 
                                   high = dfwebele['eff_nuc_char_4d'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("eff_nuc_char_4d", "@eff_nuc_char_4d"),
        ]

        
    if Feature == 'eff_nuc_char_5s' :
        color_mapper = linear_cmap(field_name = 'eff_nuc_char_5s', palette = Colorblind[8], 
                                   low = dfwebele['eff_nuc_char_5s'].min(), 
                                   high = dfwebele['eff_nuc_char_5s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("eff_nuc_char_5s", "@eff_nuc_char_5s"),
        ]

        
    if Feature == 'eff_nuc_char_5p' :
        color_mapper = linear_cmap(field_name = 'eff_nuc_char_5p', palette = Colorblind[8], 
                                   low = dfwebele['eff_nuc_char_5p'].min(), 
                                   high = dfwebele['eff_nuc_char_5p'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("eff_nuc_char_5p", "@eff_nuc_char_5p"),
        ]

        
    if Feature == 'eff_nuc_char_6s' :
        color_mapper = linear_cmap(field_name = 'eff_nuc_char_6s', palette = Colorblind[8], 
                                   low = dfwebele['eff_nuc_char_6s'].min(), 
                                   high = dfwebele['eff_nuc_char_6s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("eff_nuc_char_6s", "@eff_nuc_char_6s"),
        ]

        
    if Feature == 'atomic_radius_calculated' :
        color_mapper = linear_cmap(field_name = 'atomic_radius_calculated', palette = Colorblind[8], 
                                   low = dfwebele['atomic_radius_calculated'].min(), 
                                   high = dfwebele['atomic_radius_calculated'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_radius_calculated", "@atomic_radius_calculated pm"),
        ]

        
    if Feature == 'atomic_radius' :
        color_mapper = linear_cmap(field_name = 'atomic_radius', palette = Colorblind[8], 
                                   low = dfwebele['atomic_radius'].min(), 
                                   high = dfwebele['atomic_radius'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_radius", "@atomic_radius pm"),
        ]

        
    if Feature == 'covalent_rad' :
        color_mapper = linear_cmap(field_name = 'covalent_rad', palette = Colorblind[8], 
                                   low = dfwebele['covalent_rad'].min(), 
                                   high = dfwebele['covalent_rad'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("covalent_rad", "@covalent_rad pm"),
        ]

        
    if Feature == 'covalent_rad_emp' :
        color_mapper = linear_cmap(field_name = 'covalent_rad_emp', palette = Colorblind[8], 
                                   low = dfwebele['covalent_rad_emp'].min(), 
                                   high = dfwebele['covalent_rad_emp'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("covalent_rad_emp", "@covalent_rad_emp pm"),
        ]

        
    if Feature == 'van_der_waals_rad' :
        color_mapper = linear_cmap(field_name = 'van_der_waals_rad', palette = Colorblind[8], 
                                   low = dfwebele['van_der_waals_rad'].min(), 
                                   high = dfwebele['van_der_waals_rad'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("van_der_waals_rad", "@van_der_waals_rad pm"),
        ]


    if Feature == 'orbital_radii_s' :
        color_mapper = linear_cmap(field_name = 'orbital_radii_s', palette = Colorblind[8], 
                                   low = dfwebele['orbital_radii_s'].min(), 
                                   high = dfwebele['orbital_radii_s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("orbital_radii_s", "@orbital_radii_s /AU"),
        ]

        
    if Feature == 'orbital_radii_f' :
        color_mapper = linear_cmap(field_name = 'orbital_radii_f', palette = Colorblind[8], 
                                   low = dfwebele['orbital_radii_f'].min(), 
                                   high = dfwebele['orbital_radii_f'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("orbital_radii_f", "@orbital_radii_f /AU"),
        ]

        
    if Feature == 'orbital_radii_p' :
        color_mapper = linear_cmap(field_name = 'orbital_radii_p', palette = Colorblind[8], 
                                   low = dfwebele['orbital_radii_p'].min(), 
                                   high = dfwebele['orbital_radii_p'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("orbital_radii_p", "@orbital_radii_p /AU"),
        ]

        
    if Feature == 'orbital_radii_d' :
        color_mapper = linear_cmap(field_name = 'orbital_radii_d', palette = Colorblind[8], 
                                   low = dfwebele['orbital_radii_d'].min(), 
                                   high = dfwebele['orbital_radii_d'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("orbital_radii_d", "@orbital_radii_d /AU"),
        ]


    if Feature == 'atomic_en_allen' :
        color_mapper = linear_cmap(field_name = 'atomic_en_allen', palette = Colorblind[8], 
                                   low = dfwebele['atomic_en_allen'].min(), 
                                   high = dfwebele['atomic_en_allen'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_en_allen", "@atomic_en_allen"),
        ]

        
    if Feature == 'atomic_en_paul' :
        color_mapper = linear_cmap(field_name = 'atomic_en_paul', palette = Colorblind[8], 
                                   low = dfwebele['atomic_en_paul'].min(), 
                                   high = dfwebele['atomic_en_paul'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_en_paul", "@atomic_en_paul"),
        ]
 
        
    if Feature == 'atomic_en_allredroch' :
        color_mapper = linear_cmap(field_name = 'atomic_en_allredroch', palette = Colorblind[8], 
                                   low = dfwebele['atomic_en_allredroch'].min(), 
                                   high = dfwebele['atomic_en_allredroch'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_en_allredroch", "@atomic_en_allredroch"),
        ]

        
    if Feature == 'atomic_radius_calculated' :
        color_mapper = linear_cmap(field_name = 'atomic_radius_calculated', palette = Colorblind[8], 
                                   low = dfwebele['atomic_radius_calculated'].min(), 
                                   high = dfwebele['atomic_radius_calculated'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_radius_calculated", "@atomic_radius_calculated"),
        ]

        
    if Feature == 'atomic_en_sanderson' :
        color_mapper = linear_cmap(field_name = 'atomic_en_sanderson', palette = Colorblind[8], 
                                   low = dfwebele['atomic_en_sanderson'].min(), 
                                   high = dfwebele['atomic_en_sanderson'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_en_sanderson", "@atomic_en_sanderson"),
        ]

        
    if Feature == 'supercond_temp' :
        color_mapper = linear_cmap(field_name = 'supercond_temp', palette = Colorblind[8], 
                                   low = dfwebele['supercond_temp'].min(), 
                                   high = dfwebele['supercond_temp'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("supercond_temp", "@supercond_temp K"),
        ]

        
    if Feature == 'critical_temperature' :
        color_mapper = linear_cmap(field_name = 'critical_temperature', palette = Colorblind[8], 
                                   low = dfwebele['critical_temperature'].min(), 
                                   high = dfwebele['critical_temperature'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("critical_temperature", "@critical_temperature K"),
        ]
 
        
    if Feature == 'liquid_range' :
        color_mapper = linear_cmap(field_name = 'liquid_range', palette = Colorblind[8], 
                                   low = dfwebele['liquid_range'].min(), 
                                   high = dfwebele['liquid_range'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("liquid_range", "@liquid_range K"),
        ]
 

    if Feature == 'boiling_point' :
        color_mapper = linear_cmap(field_name = 'boiling_point', palette = Colorblind[8], 
                                   low = dfwebele['boiling_point'].min(), 
                                   high = dfwebele['boiling_point'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("boiling_point", "@boiling_point K"),
        ]
  
        
    if Feature == 'melting_point' :
        color_mapper = linear_cmap(field_name = 'melting_point', palette = Colorblind[8], 
                                   low = dfwebele['melting_point'].min(), 
                                   high = dfwebele['melting_point'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("melting_point", "@melting_point K"),
        ]

        
    if Feature == 'atomic_hfu' :
        color_mapper = linear_cmap(field_name = 'atomic_hfu', palette = Colorblind[8], 
                                   low = dfwebele['atomic_hfu'].min(), 
                                   high = dfwebele['atomic_hfu'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_hfu", "@atomic_hfu kJ/mol"),
        ]

        
    if Feature == 'atomic_hvap' :
        color_mapper = linear_cmap(field_name = 'atomic_hvap', palette = Colorblind[8], 
                                   low = dfwebele['atomic_hvap'].min(), 
                                   high = dfwebele['atomic_hvap'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_hvap", "@atomic_hvap kJ/mol"),
        ]
  
        
    if Feature == 'atomic_hatm' :
        color_mapper = linear_cmap(field_name = 'atomic_hatm', palette = Colorblind[8], 
                                   low = dfwebele['atomic_hatm'].min(), 
                                   high = dfwebele['atomic_hatm'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_hatm", "@atomic_hatm kJ/mol"),
        ]

        
    if Feature == 'Electron_bin_En_1s' :
        color_mapper = linear_cmap(field_name = 'Electron_bin_En_1s', palette = Colorblind[8], 
                                   low = dfwebele['Electron_bin_En_1s'].min(), 
                                   high = dfwebele['Electron_bin_En_1s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("Electron_bin_En_1s", "@Electron_bin_En_1s eV"),
        ]


    if Feature == 'Electron_bin_En_2s' :
        color_mapper = linear_cmap(field_name = 'Electron_bin_En_2s', palette = Colorblind[8], 
                                   low = dfwebele['Electron_bin_En_2s'].min(), 
                                   high = dfwebele['Electron_bin_En_2s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("Electron_bin_En_2s", "@Electron_bin_En_2s eV"),
        ]


    if Feature == 'Ionisation_En_1st' :
        color_mapper = linear_cmap(field_name = 'Ionisation_En_1st', palette = Colorblind[8], 
                                   low = dfwebele['Ionisation_En_1st'].min(), 
                                   high = dfwebele['Ionisation_En_1st'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("Ionisation_En_1st", "@Ionisation_En_1st (Enthalpy/kJ mol‑1)"),
        ]

        
    if Feature == 'Ionisation_En_2nd' :
        color_mapper = linear_cmap(field_name = 'Ionisation_En_2nd', palette = Colorblind[8], 
                                   low = dfwebele['Ionisation_En_2nd'].min(), 
                                   high = dfwebele['Ionisation_En_2nd'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("Ionisation_En_2nd", "@Ionisation_En_2nd (Enthalpy/kJ mol‑1)"),
        ]

        
    if Feature == 'Ionisation_En_3rd' :
        color_mapper = linear_cmap(field_name = 'Ionisation_En_3rd', palette = Colorblind[8], 
                                   low = dfwebele['Ionisation_En_3rd'].min(), 
                                   high = dfwebele['Ionisation_En_3rd'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("Ionisation_En_3rd", "@Ionisation_En_3rd (Enthalpy/kJ mol‑1)"),
        ]

    p2.rect("group", "period", 0.90, 0.90, source= source, fill_alpha=0.6,color=color_mapper)
    text_props = {"source":source, "text_align": "left", "text_baseline": "middle", "text_color" : "black"}

    x1 = dodge("group", -0.4, range=p2.x_range)

    r1 = p2.text(x=x1, y="period", text="symbol", **text_props)
    r1.glyph.text_font_style="bold"

    r1 = p2.text(x=x1, y=dodge("period", 0.3, range=p2.y_range), text="atomic number", **text_props)
    r1.glyph.text_font_size="8pt"

    r1 = p2.text(x=x1, y=dodge("period", -0.35, range=p2.y_range), text="name", **text_props)
    r1.glyph.text_font_size="5pt"
    #Defines color bar
    color_bar = ColorBar(color_mapper=color_mapper['transform'],
                         formatter = NumeralTickFormatter(format='0.0000'),label_standoff = 13, width=8, location=(0,0))
    # Set color_bar location
    p2.add_layout(color_bar, 'right')
    p2.outline_line_color = None
    p2.grid.grid_line_color = None
    p2.axis.axis_line_color = None
    p2.axis.major_tick_line_color = None
    p2.axis.major_label_standoff = 0
    p2.axis.visible = False
    output_notebook(hide_banner=True)
    show(p2,notebook_handle=True)
    push_notebook()

def showfigf(Feature):
    """
    Generates an interactive heatmap with dropdown menu for  atomic_properites_dft module atomic features(Spin= "False")
    """
    return interact(ptableplotgenf, Feature = Feature)

def showfigt(Feature):
    """
    Generates an interactive heatmap with dropdown menu for  atomic_properites_dft module atomic features(Spin= "True")
    """
    return interact(ptableplotgent, Feature = Feature)

def showfiglda(Feature):
    """
    Generates an interactive heatmap with dropdown menu for  atomic_properites_lda2015 module atomic features.
    """
    return interact(ptableplotgenlda, Feature = Feature)

def showfigmatmin(Feature):
    """
    Generates an interactive heatmap with dropdown menu for  atomic_properites_magpie module atomic features.
    """
    return interact(ptableplotgenmatmin, Feature = Feature)
    
def showfigpymat(Feature):
    """
    Generates an interactive heatmap with dropdown menu for  atomic_properites_pymat module atomic features.
    """
    return interact(ptableplotgenpymat, Feature = Feature)
    
def showfigwebele(Feature):
    """
    Generates an interactive heatmap with dropdown menu for  atomic_properites_webele module atomic features.
    """
    return interact(ptableplotgenwebele, Feature = Feature)

def lda(df3,df4):
    """
    Adds atomic_properites_lda2015 module atomic features to previously cleaned pandas dataframe imported from bokeh
    """
    df3.set_index('symbol',inplace=True)
    for r,c in df4.iterrows():
        for j in df3.index:
            if (r==j):
                df3.loc[j,"atomic_homo"] = df4.loc[j,"E_HOMO"]
                df3.loc[j,"atomic_lumo"] = df4.loc[j,"E_LUMO"]
                df3.loc[j,"atomic_ip"] = df4.loc[j,"IP"]
                df3.loc[j,"atomic_ea"] = df4.loc[j,"EA"]
                df3.loc[j,"atomic_r_s"] = df4.loc[j,"r_s"]*100
                df3.loc[j,"atomic_r_p"] = df4.loc[j,"r_p"]*100
                df3.loc[j,"atomic_r_d"] = df4.loc[j,"r_d"]*100
    
    globals()['dflda'] = df3



def f(df3,df4):
    """
    Adds atomic_properites_dft module atomic features(Spin= "False") to previously cleaned pandas dataframe imported from bokeh
    """
    df3.set_index('symbol',inplace=True)
    for r,c in df4.iterrows():
        for j in df3.index:
            if (r==j):   
                df3.loc[j,"atomic_hfomo"] = df4.loc[j,"atomic_hfomo"]*6.242e+18
                df3.loc[j,"atomic_hpomo"] = df4.loc[j,"atomic_hpomo"]*6.242e+18
                df3.loc[j,"atomic_lfumo"] = df4.loc[j,"atomic_lfumo"]*6.242e+18
                df3.loc[j,"atomic_lpumo"] = df4.loc[j,"atomic_lpumo"]*6.242e+18
                df3.loc[j,"atomic_ip_by_half_charged_homo"] = df4.loc[j,"atomic_ip_by_half_charged_homo"]*6.242e+18
                df3.loc[j,"atomic_ea_by_half_charged_homo"] = df4.loc[j,"atomic_ea_by_half_charged_homo"]*6.242e+18
                df3.loc[j,"atomic_ip"] = df4.loc[j,"atomic_ip_by_energy_difference"]*6.242e+18
                df3.loc[j,"atomic_ea"] = df4.loc[j,"atomic_ea_by_energy_difference"]*6.242e+18
                df3.loc[j,"atomic_r_s_neg_1"] = df4.loc[j,"r_s_neg_1.0"]*100
                df3.loc[j,"atomic_r_p_neg_1"] = df4.loc[j,"r_p_neg_1.0"]*100
                df3.loc[j,"atomic_r_d_neg_1"] = df4.loc[j,"r_d_neg_1.0"]*100
                df3.loc[j,"atomic_r_val_neg_1"] = df4.loc[j,"r_val_neg_1.0"]*100
                df3.loc[j,"atomic_r_s_neg_05"] = df4.loc[j,"r_s_neg_0.5"]*100
                df3.loc[j,"atomic_r_p_neg_05"] = df4.loc[j,"r_p_neg_0.5"]*100
                df3.loc[j,"atomic_r_d_neg_05"] = df4.loc[j,"r_d_neg_0.5"]*100
                df3.loc[j,"atomic_r_val_neg_05"] = df4.loc[j,"r_val_neg_0.5"]*100
                df3.loc[j,"atomic_r_s"] = df4.loc[j,"r_s_0.0"]*100
                df3.loc[j,"atomic_r_p"] = df4.loc[j,"r_p_0.0"]*100
                df3.loc[j,"atomic_r_d"] = df4.loc[j,"r_d_0.0"]*100
                df3.loc[j,"atomic_r_val"] = df4.loc[j,"r_val_0.0"]*100
                df3.loc[j,"atomic_r_s_05"] = df4.loc[j,"r_s_0.5"]*100
                df3.loc[j,"atomic_r_p_05"] = df4.loc[j,"r_p_0.5"]*100
                df3.loc[j,"atomic_r_d_05"] = df4.loc[j,"r_d_0.5"]*100
                df3.loc[j,"atomic_r_val_05"] = df4.loc[j,"r_val_0.5"]*100
                df3.loc[j,"atomic_r_s_1"] = df4.loc[j,"r_s_1.0"]*100
                df3.loc[j,"atomic_r_p_1"] = df4.loc[j,"r_p_1.0"]*100
                df3.loc[j,"atomic_r_d_1"] = df4.loc[j,"r_d_1.0"]*100
                df3.loc[j,"atomic_r_val_1"] = df4.loc[j,"r_val_1.0"]*100
    
    globals()['dffa'] = df3
                
def t(df3,df4):
    """
    Adds atomic_properites_dft module atomic features(Spin= "True") to previously cleaned pandas dataframe imported from bokeh
    """
    df3.set_index('symbol',inplace=True)
    for r,c in df4.iterrows():
        for j in df3.index:
            if (r==j):
                df3.loc[j,"atomic_hfomo"] = df4.loc[j,"atomic_hfomo"]*6.242e+18
                df3.loc[j,"atomic_hpomo"] = df4.loc[j,"atomic_hpomo"]*6.242e+18
                df3.loc[j,"atomic_lfumo"] = df4.loc[j,"atomic_lfumo"]*6.242e+18
                df3.loc[j,"atomic_lpumo"] = df4.loc[j,"atomic_lpumo"]*6.242e+18
                df3.loc[j,"atomic_ip_by_half_charged_homo"] = df4.loc[j,"atomic_ip_by_half_charged_homo"]*6.242e+18
                df3.loc[j,"atomic_ea_by_half_charged_homo"] = df4.loc[j,"atomic_ea_by_half_charged_homo"]*6.242e+18
                df3.loc[j,"atomic_ip"] = df4.loc[j,"atomic_ip_by_energy_difference"]*6.242e+18
                df3.loc[j,"atomic_ea"] = df4.loc[j,"atomic_ea_by_energy_difference"]*6.242e+18
                df3.loc[j,"atomic_r_up_s_neg_1"] = df4.loc[j,"r_up_s_neg_1.0"]*100
                df3.loc[j,"atomic_r_dn_s_neg_1"] = df4.loc[j,"r_dn_s_neg_1.0"]*100
                df3.loc[j,"atomic_r_up_p_neg_1"] = df4.loc[j,"r_up_p_neg_1.0"]*100
                df3.loc[j,"atomic_r_dn_p_neg_1"] = df4.loc[j,"r_dn_p_neg_1.0"]*100
                df3.loc[j,"atomic_r_dn_d_neg_1"] = df4.loc[j,"r_dn_d_neg_1.0"]*100
                df3.loc[j,"atomic_r_dn_d_neg_1"] = df4.loc[j,"r_dn_d_neg_1.0"]*100
                df3.loc[j,"atomic_r_up_val_neg_1"] = df4.loc[j,"r_up_val_neg_1.0"]*100
                df3.loc[j,"atomic_r_dn_val_neg_1"] = df4.loc[j,"r_dn_val_neg_1.0"]*100
                df3.loc[j,"atomic_r_up_s_neg_05"] = df4.loc[j,"r_up_s_neg_0.5"]*100
                df3.loc[j,"atomic_r_dn_s_neg_05"] = df4.loc[j,"r_dn_s_neg_0.5"]*100
                df3.loc[j,"atomic_r_up_p_neg_05"] = df4.loc[j,"r_up_p_neg_0.5"]*100
                df3.loc[j,"atomic_r_dn_p_neg_05"] = df4.loc[j,"r_dn_p_neg_0.5"]*100
                df3.loc[j,"atomic_r_up_d_neg_05"] = df4.loc[j,"r_up_d_neg_0.5"]*100
                df3.loc[j,"atomic_r_dn_d_neg_05"] = df4.loc[j,"r_dn_d_neg_0.5"]*100
                df3.loc[j,"atomic_r_up_val_neg_05"] = df4.loc[j,"r_up_val_neg_0.5"]*100
                df3.loc[j,"atomic_r_dn_val_neg_05"] = df4.loc[j,"r_dn_val_neg_0.5"]*100
                df3.loc[j,"atomic_r_up_s"] = df4.loc[j,"r_up_s_0.0"]*100
                df3.loc[j,"atomic_r_dn_s"] = df4.loc[j,"r_dn_s_0.0"]*100
                df3.loc[j,"atomic_r_up_p"] = df4.loc[j,"r_up_p_0.0"]*100
                df3.loc[j,"atomic_r_dn_p"] = df4.loc[j,"r_dn_p_0.0"]*100
                df3.loc[j,"atomic_r_up_d"] = df4.loc[j,"r_up_d_0.0"]*100
                df3.loc[j,"atomic_r_dn_d"] = df4.loc[j,"r_dn_d_0.0"]*100
                df3.loc[j,"atomic_r_up_val"] = df4.loc[j,"r_up_val_0.0"]*100
                df3.loc[j,"atomic_r_dn_val"] = df4.loc[j,"r_dn_val_0.0"]*100
                df3.loc[j,"atomic_r_up_s_05"] = df4.loc[j,"r_up_s_0.5"]*100
                df3.loc[j,"atomic_r_dn_s_05"] = df4.loc[j,"r_dn_s_0.5"]*100
                df3.loc[j,"atomic_r_up_p_05"] = df4.loc[j,"r_up_p_0.5"]*100
                df3.loc[j,"atomic_r_dn_p_05"] = df4.loc[j,"r_dn_p_0.5"]*100
                df3.loc[j,"atomic_r_up_d_05"] = df4.loc[j,"r_up_d_0.5"]*100
                df3.loc[j,"atomic_r_dn_d_05"] = df4.loc[j,"r_dn_d_0.5"]*100
                df3.loc[j,"atomic_r_up_val_05"] = df4.loc[j,"r_up_val_0.5"]*100
                df3.loc[j,"atomic_r_dn_val_05"] = df4.loc[j,"r_dn_val_0.5"]*100
                df3.loc[j,"atomic_r_up_s_1"] = df4.loc[j,"r_up_s_1.0"]*100
                df3.loc[j,"atomic_r_dn_s_1"] = df4.loc[j,"r_dn_s_1.0"]*100
                df3.loc[j,"atomic_r_up_p_1"] = df4.loc[j,"r_up_p_1.0"]*100
                df3.loc[j,"atomic_r_dn_p_1"] = df4.loc[j,"r_dn_p_1.0"]*100
                df3.loc[j,"atomic_r_up_d_1"] = df4.loc[j,"r_up_d_1.0"]*100
                df3.loc[j,"atomic_r_dn_d_1"] = df4.loc[j,"r_dn_d_1.0"]*100
                df3.loc[j,"atomic_r_up_val_1"] = df4.loc[j,"r_up_val_1.0"]*100
                df3.loc[j,"atomic_r_dn_val_1"] = df4.loc[j,"r_dn_val_1.0"]*100
                
    globals()['dftr'] = df3



def matmin(df3,df4):
    """
    Adds atomic_properties_matminer module atomic features to previously cleaned pandas dataframe imported from bokeh
    """
    df3.set_index('symbol',inplace=True)
    for r,c in df4.iterrows():
        for j in df3.index:
            if (r==j):
                df3.loc[j,"atomic_ICSD_vol"] = float(df4.loc[j,"atomic_ICSD_vol"])
                df3.loc[j,"atomic_cpmass"] =  float(df4.loc[j,"atomic_cpmass"])
                df3.loc[j,"atomic_cpmolar"] =  float(df4.loc[j,"atomic_cpmolar"])
                df3.loc[j,"atomic_density"] =  float(df4.loc[j,"atomic_density"])
                df3.loc[j,"atomic_ea"] =  float(df4.loc[j,"atomic_ea"])
                df3.loc[j,"atomic_en"] =  float(df4.loc[j,"atomic_en"])
                df3.loc[j,"atomic_en_allen"] =  float(df4.loc[j,"atomic_en_allen"])
                df3.loc[j,"atomic_hfu"] =  float(df4.loc[j,"atomic_hfu"])
                df3.loc[j,"atomic_hhip"] =  float(df4.loc[j,"atomic_hhip"])
                df3.loc[j,"atomic_hhir"] =  float(df4.loc[j,"atomic_hhir"])
                df3.loc[j,"atomic_hvap"] =  float(df4.loc[j,"atomic_hvap"])
                df3.loc[j,"atomic_ie_1"] =  float(df4.loc[j,"atomic_ie_1"])
                df3.loc[j,"atomic_ie_2"] =  float(df4.loc[j,"atomic_ie_2"])
                df3.loc[j,"atomic_ndunf"] =  float(df4.loc[j,"atomic_ndunf"])
                df3.loc[j,"atomic_ndval"] =  float(df4.loc[j,"atomic_ndval"])
                df3.loc[j,"atomic_nfunf"] =  float(df4.loc[j,"atomic_nfunf"])
                df3.loc[j,"atomic_nfval"] =  float(df4.loc[j,"atomic_nfval"])
                df3.loc[j,"atomic_npunf"] =  float(df4.loc[j,"atomic_npunf"])
                df3.loc[j,"atomic_npval"] =  float(df4.loc[j,"atomic_npval"])
                df3.loc[j,"atomic_nsunf"] =  float(df4.loc[j,"atomic_nsunf"])
                df3.loc[j,"atomic_nsval"] =  float(df4.loc[j,"atomic_nsval"])
                df3.loc[j,"atomic_nunf"] =  float(df4.loc[j,"atomic_nunf"])
                df3.loc[j,"atomic_nval"] =  float(df4.loc[j,"atomic_nval"])
                df3.loc[j,"atomic_phi"] =  float(df4.loc[j,"atomic_phi"])
                df3.loc[j,"atomic_pol"] =  float(df4.loc[j,"atomic_pol"])
                df3.loc[j,"atomic_pp_r_d"] =  float(df4.loc[j,"atomic_pp_r_d"])
                df3.loc[j,"atomic_pp_r_p"] =  float(df4.loc[j,"atomic_pp_r_p"])
                df3.loc[j,"atomic_pp_r_pi"] =  float(df4.loc[j,"atomic_pp_r_pi"])
                df3.loc[j,"atomic_pp_r_s"] =  float(df4.loc[j,"atomic_pp_r_s"])
                df3.loc[j,"atomic_pp_r_sig"] =  float(df4.loc[j,"atomic_pp_r_sig"])
                df3.loc[j,"atomic_radius"] =  float(df4.loc[j,"atomic_radius"])
                df3.loc[j,"atomic_vdw_radius"] =  float(df4.loc[j,"atomic_vdw_radius"])
                df3.loc[j,"atomic_volume"] =  float(df4.loc[j,"atomic_volume"])
                df3.loc[j,"atomic_weight"] =  float(df4.loc[j,"atomic_weight"])
                df3.loc[j,"atomic_ws3"] =  float(df4.loc[j,"atomic_ws3"])
                df3.loc[j,"boiling_temp"] =  float(df4.loc[j,"boiling_temp"])
                df3.loc[j,"bulk_modulus"] =  float(df4.loc[j,"bulk_modulus"])
                df3.loc[j,"covalent_radius"] =  float(df4.loc[j,"covalent_radius"])
                df3.loc[j,"gs_bandgap"] =  float(df4.loc[j,"gs_bandgap"])
                df3.loc[j,"gs_bcclatparam"] =  float(df4.loc[j,"gs_bcclatparam"])
                df3.loc[j,"gs_energy_pa"] =  float(df4.loc[j,"gs_energy_pa"])
                df3.loc[j,"gs_fcclatparam"] =  float(df4.loc[j,"gs_fcclatparam"])
                df3.loc[j,"gs_mag_mom"] =  float(df4.loc[j,"gs_mag_mom"])
                df3.loc[j,"gs_volume_pa"] =  float(df4.loc[j,"gs_volume_pa"])
                df3.loc[j,"melting_point"] =  float(df4.loc[j,"melting_point"])
                df3.loc[j,"mendeleev_no"] =  float(df4.loc[j,"mendeleev_no"])
                df3.loc[j,"micracle_radius"] =  float(df4.loc[j,"micracle_radius"])
                df3.loc[j,"molar_volume"] =  float(df4.loc[j,"molar_volume"])
                df3.loc[j,"shear_mod"] =  float(df4.loc[j,"shear_mod"])
                df3.loc[j,"thermal_cond"] =  float(df4.loc[j,"thermal_cond"])
                df3.loc[j,"thermal_cond_log"] =  float(df4.loc[j,"thermal_cond_log"])
                #df3["atomic_ie"] = df4["atomic_ie"]
                #df3["atomic_oxstates"] = df4["atomic_oxstates"]
                df3.loc[j,"periodict_column"] = float(df4.loc[j,"periodict_column"])
                df3.loc[j,"periodict_row"] =  float(df4.loc[j,"periodict_row"])
                df3.loc[j,"atomic_isalkali"] =  float(df4.loc[j,"atomic_isalkali"])
                df3.loc[j,"atomic_isdblock"] =  float(df4.loc[j,"atomic_isdblock"])
                df3.loc[j,"atomic_isfblock"] =  float(df4.loc[j,"atomic_isfblock"])
                df3.loc[j,"atomic_ismetal"] =  float(df4.loc[j,"atomic_ismetal"])
                df3.loc[j,"atomic_ismetalloid"] =  float(df4.loc[j,"atomic_ismetalloid"])
                df3.loc[j,"atomic_isnonmetal"] =  float(df4.loc[j,"atomic_isnonmetal"])

    globals()['dfmatm'] = df3
                    
def pymat(df3,df4):
    """
    Adds atomic_properites_pymat module atomic features to previously cleaned pandas dataframe imported from bokeh
    """
    df3.set_index('symbol',inplace=True)
    for r,c in df4.iterrows():
        for j in df3.index:
            if (r==j):
                #df3.loc[j,"atomic_orbitals"] = df4.loc[j,"Atomic orbitals"]
                df3.loc[j,"atomic_radius"] = float(df4.loc[j,"Atomic radius"])
                df3.loc[j,"atomic_radius_calculated"] = float(df4.loc[j,"Atomic radius calculated"])
                df3.loc[j,"boiling_point"] = float(df4.loc[j,"Boiling point"])
                df3.loc[j,"brinell_hardness"] = float(df4.loc[j,"Brinell hardness"])
                df3.loc[j,"bulk_modulus"] = float(df4.loc[j,"Bulk modulus"])
                df3.loc[j,"coefficient_olte"] = float(df4.loc[j,"Coefficient of linear thermal expansion"])
                #df3.loc[j,"common_ox_states"] = df4.loc[j,"Common oxidation states"]
                df3.loc[j,"critical_temperature"] = float(df4.loc[j,"Critical temperature"])
                df3.loc[j,"density_of_solid"] = float(df4.loc[j,"Density of solid"])
                #df3.loc[j,"ionic_radii"] = df4.loc[j,"Ionic radii"]
                df3.loc[j,"liquid_range"] = float(df4.loc[j,"Liquid range"])
                df3.loc[j,"mendeleev_no"] = float(df4.loc[j,"Mendeleev no"])
                df3.loc[j,"mineral_hardness"] = float(df4.loc[j,"Mineral hardness"])
                df3.loc[j,"molar_volume"] = float(df4.loc[j,"Molar volume"])
                #df3["oxidation_states"] = df4["Oxidation states"]
                df3.loc[j,"atomic_refelctivity"] = float(df4.loc[j,"Reflectivity"])
                df3.loc[j,"rigidity_modulus"] = float(df4.loc[j,"Rigidity modulus"])
                #df3["shannon_radii"] = df4["Shannon radii"]
            
                df3.loc[j,"van_der_waals_rad"] = float(df4.loc[j,"Van der waals radius"])
                df3.loc[j,"velocity_of_sound"] = float(df4.loc[j,"Velocity of sound"])
                df3.loc[j,"vickers_hardness"] = float(df4.loc[j,"Vickers hardness"])
                df3.loc[j,"poisson_ratio"] = float(df4.loc[j,"Poissons ratio"])
                df3.loc[j,"x"] = float(df4.loc[j,"X"])
                df3.loc[j,"youngs_modulus"] = float(df4.loc[j,"Youngs modulus"])
                df3.loc[j,"metallic_radius"] = float(df4.loc[j,"Metallic radius"])
                df3.loc[j,"iupac_ordering"] = float(df4.loc[j,"IUPAC ordering"])
                #df3["icsd_oxd_states"] = df4["ICSD oxidation states"]
                #df3["nmr_quadrapole_mom"] = df4["NMR Quadrupole Moment"]
                df3.loc[j,"max_oxd_state"] = float(df4.loc[j,"Max oxidation state"])
                df3.loc[j,"min_oxd_state"] = float(df4.loc[j,"Min oxidation state"])
                #df3["ionic_radii_hs"] = df4["Ionic radii hs"]
                #df3["ionic_radii_ls"] = df4["Ionic radii ls"]
                try:
                    temp=df4.loc[j,"Electrical resistivity"]
                    val=re.findall(r"[-+]?\d*\.\d+|\d+",temp)
                    df3.loc[j,"electrical_resistivity"] = float(val[0])
                except TypeError:
                    df3.loc[j,"electrical_resistivity"] = df4.loc[j,"Electrical resistivity"]
                except IndexError:
                    df3.loc[j,"electrical_resistivity"] = np.nan
            
                try:
                    temp=df4.loc[j,"Melting point"]
                    val=re.findall(r"[-+]?\d*\.\d+|\d+",temp)
                    df3.loc[j,"melting_point"] = float(val[0])
                except TypeError:
                    df3.loc[j,"melting_point"] = df4.loc[j,"Melting point"]
                try:
                    temp=df4.loc[j,"Superconduction temperature"]
                    val=re.findall(r"[-+]?\d*\.\d+|\d+",temp)
                    df3.loc[j,"supercond_temp"] = float(val[0])
                except TypeError:
                    df3.loc[j,"supercond_temp"] = df4.loc[j,"Superconduction temperature"]
            
                try:
                    temp=df4.loc[j,"Thermal conductivity"]
                    val=re.findall(r"[-+]?\d*\.\d+|\d+",temp)
                    df3.loc[j,"thermal_cond"] = float(val[0])
                except TypeError:
                    df3.loc[j,"thermal_cond"] = df4.loc[j,"Thermal conductivity"]
            
                try:
                    df3.loc[j,"atomic_orbitals_1s"] = float(df4.loc[j,"Atomic orbitals"]['1s'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_1s"] = np.nan
                try:
                    df3.loc[j,"atomic_orbitals_2s"] = float(df4.loc[j,"Atomic orbitals"]['2s'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_2s"] = np.nan
                try:
                    df3.loc[j,"atomic_orbitals_2p"] = float(df4.loc[j,"Atomic orbitals"]['2p'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_2p"] = np.nan
                try:
                    df3.loc[j,"atomic_orbitals_3s"] = float(df4.loc[j,"Atomic orbitals"]['3s'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_3s"] = np.nan
                try:
                    df3.loc[j,"atomic_orbitals_3p"] = float(df4.loc[j,"Atomic orbitals"]['3p'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_3p"] = np.nan
                try:
                    df3.loc[j,"atomic_orbitals_3d"] = float(df4.loc[j,"Atomic orbitals"]['3d'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_3d"] = np.nan
                try:
                    df3.loc[j,"atomic_orbitals_4s"] = float(df4.loc[j,"Atomic orbitals"]['4s'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_4s"] = np.nan
                try:
                    df3.loc[j,"atomic_orbitals_4p"] = float(df4.loc[j,"Atomic orbitals"]['4p'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_4p"] = np.nan
                try:
                    df3.loc[j,"atomic_orbitals_4d"] = float(df4.loc[j,"Atomic orbitals"]['4d'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_4d"] = np.nan
                try:
                    df3.loc[j,"atomic_orbitals_5s"] = float(df4.loc[j,"Atomic orbitals"]['5s'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_5s"] = np.nan
                try:
                    df3.loc[j,"atomic_orbitals_5p"] = float(df4.loc[j,"Atomic orbitals"]['5p'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_5p"] = np.nan
                try:
                    df3.loc[j,"atomic_orbitals_5d"] = float(df4.loc[j,"Atomic orbitals"]['5d'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_5d"] = np.nan
                try:
                    df3.loc[j,"atomic_orbitals_6s"] = float(df4.loc[j,"Atomic orbitals"]['6s'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_6s"] = np.nan
                try:
                    df3.loc[j,"atomic_orbitals_6p"] = float(df4.loc[j,"Atomic orbitals"]['6p'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_6p"] = np.nan
                try:
                    df3.loc[j,"atomic_orbitals_6d"] = float(df4.loc[j,"Atomic orbitals"]['6d'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_6d"] = np.nan
                try:
                    df3.loc[j,"atomic_orbitals_7s"] = float(df4.loc[j,"Atomic orbitals"]['7s'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_7s"] = np.nan
                try:
                    df3.loc[j,"atomic_orbitals_4f"] = float(df4.loc[j,"Atomic orbitals"]['4f'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_4f"] = np.nan
                try:
                    df3.loc[j,"atomic_orbitals_5f"] = float(df4.loc[j,"Atomic orbitals"]['5f'])
                except (KeyError,TypeError) as error:
                    df3.loc[j,"atomic_orbitals_5f"] = np.nan
                    
    globals()['dfpymat'] = df3
    
def webele(df3,df4):
    """
    Adds atomic_properites_webele module atomic features to previously cleaned pandas dataframe imported from bokeh
    """
    for i in df3.index:
        df3.loc[i,'name']= df3.loc[i,'name'].lower()
    df3.set_index('name',inplace=True)

    for r,c in df4.iterrows():
        for j in df3.index:
            if (j==r):
                try:
                    df3.loc[j,'atomic_radius_calculated'] = float(df4.loc[j,'atom_radii']
                                                                  ['Atomic radius (calculated)']
                                                                  .strip())
                except ValueError:
                    df3.loc[j,'atomic_radius_calculated'] = np.nan
                try:
                    df3.loc[j,'atomic_radius'] = float(df4.loc[j,'atom_radii']
                                                       ['Atomic radius (empirical)'].strip())
                except ValueError:
                    df3.loc[j,'atomic_radius'] = np.nan

                try:
                    df3.loc[j,'atomic_en_allen'] = float(df4.loc[j,'electronegativity']
                                                       ['Allen electronegativity'].strip())
                except AttributeError:
                    df3.loc[j,'atomic_en_allen'] = np.nan
                try:
                    df3.loc[j,'atomic_en_paul'] = float(df4.loc[j,'electronegativity']
                                                      ['Pauling electronegativity'].strip())
                except AttributeError:
                    df3.loc[j,'atomic_en_paul'] = np.nan
                try:
                    df3.loc[j,'atomic_en_allredroch'] = float(df4.loc[j,'electronegativity']
                                                          ['Allred Rochow electronegativity'].strip())
                except AttributeError:
                    df3.loc[j,'atomic_en_allredroch'] = np.nan
                try:
                    df3.loc[j,'atomic_en_sanderson'] = float(df4.loc[j,'electronegativity']
                                                             ['Sanderson electronegativity'].strip())
                except AttributeError:
                    df3.loc[j,'atomic_en_sanderson'] = np.nan
                
                try:
                    df3.loc[j,'atomic_hfu'] = df4.loc[j,'thermo_chem']['Enthalpy of fusion']
                except:
                    df3.loc[j,'atomic_hfu'] = np.nan
                try:
                    df3.loc[j,'atomic_hvap'] = df4.loc[j,'thermo_chem']['Enthalpy of vaporisation']
                except:
                    df3.loc[j,'atomic_hvap'] = np.nan
                try:
                    df3.loc[j,'atomic_hatm'] = df4.loc[j,'thermo_chem']['Enthalpy of atomisation']
                except:
                    df3.loc[j,'atomic_hatm'] = np.nan


                try:
                    df3.loc[j,'eff_nuc_char_2s'] = float(df4.loc[j,'Eff_Nuc_Char']['2s'])
                except ValueError:
                    df3.loc[j,'eff_nuc_char_2s'] = np.nan
                try:
                    df3.loc[j,'eff_nuc_char_3s'] = float(df4.loc[j,'Eff_Nuc_Char']['3s'])
                except ValueError:
                    df3.loc[j,'eff_nuc_char_3s'] = np.nan
                try:
                    df3.loc[j,'eff_nuc_char_3p'] = float(df4.loc[j,'Eff_Nuc_Char']['3p'])
                except ValueError:
                    df3.loc[j,'eff_nuc_char_3p'] = np.nan
                try:
                    df3.loc[j,'eff_nuc_char_4s'] = float(df4.loc[j,'Eff_Nuc_Char']['4s'])
                except ValueError:
                    df3.loc[j,'eff_nuc_char_4s'] = np.nan
                try:
                    df3.loc[j,'eff_nuc_char_4p'] = float(df4.loc[j,'Eff_Nuc_Char']['4p'])
                except ValueError:
                    df3.loc[j,'eff_nuc_char_4p'] = np.nan
                try:
                    df3.loc[j,'eff_nuc_char_4d'] = float(df4.loc[j,'Eff_Nuc_Char']['4d'])
                except ValueError:
                    df3.loc[j,'eff_nuc_char_4d'] = np.nan
                try:
                    df3.loc[j,'eff_nuc_char_5s'] = float(df4.loc[j,'Eff_Nuc_Char']['5s'])
                except ValueError:
                    df3.loc[j,'eff_nuc_char_5s'] = np.nan
                try:
                    df3.loc[j,'eff_nuc_char_5p'] = float(df4.loc[j,'Eff_Nuc_Char']['5p'])
                except ValueError:
                    df3.loc[j,'eff_nuc_char_5p'] = np.nan
                try:
                    df3.loc[j,'eff_nuc_char_5s'] = float(df4.loc[j,'Eff_Nuc_Char']['5s'])
                except ValueError:
                    df3.loc[j,'eff_nuc_char_5s'] = np.nan
                try:
                    df3.loc[j,'eff_nuc_char_6s'] = float(df4.loc[j,'Eff_Nuc_Char']['6s'])
                except ValueError:
                    df3.loc[j,'eff_nuc_char_6s'] = np.nan
                    
                try:
                    df3.loc[j,'density_of_solid'] = float(df4.loc[j,'Density_of_solid'].strip())
                except AttributeError:
                    df3.loc[j,'density_of_solid'] = np.nan
                try:
                    df3.loc[j,'molar_vol'] = float(df4.loc[j,'Molar_vol'].strip())
                except AttributeError:
                    df3.loc[j,'molar_vol'] = np.nan
                try:
                    df3.loc[j,'youngs_mod'] = float(df4.loc[j,'Youngs_mod'].strip())
                except AttributeError:
                    df3.loc[j,'youngs_mod'] = np.nan
                try:
                    df3.loc[j,'rigidity_mod'] = float(df4.loc[j,'Rigidity_mod'].strip())
                except AttributeError:
                    df3.loc[j,'rigidity_mod'] = np.nan
                try:
                    df3.loc[j,'bulk_mod'] = float(df4.loc[j,'Bulk_mod'].strip())
                except AttributeError:
                    df3.loc[j,'bulk_mod'] = np.nan
                try:
                    df3.loc[j,'poissons_ratio'] = float(df4.loc[j,'Poissons_ratio'].strip())
                except AttributeError:
                    df3.loc[j,'poissons_ratio'] = np.nan
                try:
                    df3.loc[j,'mineral_hardness'] = float(df4.loc[j,'Mineral_hardness'].strip())
                except AttributeError:
                    df3.loc[j,'mineral_hardness'] = np.nan
                try:
                    df3.loc[j,'brinell_hardness'] = float(df4.loc[j,'Brinell_hardness'].strip())
                except AttributeError:
                    df3.loc[j,'brinell_hardness'] = np.nan
                try:
                    df3.loc[j,'vickers_hardness'] = float(df4.loc[j,'Vickers_hardness'].strip())
                except AttributeError:
                    df3.loc[j,'vickers_hardness'] = np.nan
                try:
                    df3.loc[j,'electrical_resistivity'] = float(df4.loc[j,'Electrical_resistivity'].strip())
                except AttributeError:
                    df3.loc[j,'electrical_resistivity'] = np.nan
                try:
                    df3.loc[j,'thermal_conductivity'] = float(df4.loc[j,'Thermal_conductivity'].strip())
                except AttributeError:
                    df3.loc[j,'thermal_conductivity'] = np.nan
                try:
                    df3.loc[j,'coeff_of_lte'] = float(df4.loc[j,'Coeff_of_lte'].strip())
                except AttributeError:
                    df3.loc[j,'coeff_of_lte'] = np.nan
                try:
                    df3.loc[j,'reflectivity'] = float(df4.loc[j,'Reflectivity'].strip())
                except AttributeError:
                    df3.loc[j,'reflectivity'] = np.nan
                try:
                    df3.loc[j,'velocity_of_sound'] = float(df4.loc[j,'Velocity_of_sound'].strip())
                except AttributeError:
                    df3.loc[j,'velocity_of_sound'] = np.nan
                try:
                    df3.loc[j,'electron_aff'] = float(df4.loc[j,'Electron_aff'].strip())
                except AttributeError:
                    df3.loc[j,'electron_aff'] = np.nan
                

                try:
                    df3.loc[j,'covalent_rad'] = float(df4.loc[j,'atom_radii']
                                                      ['Covalent radius (2008 values)'].strip())
                except ValueError:
                    df3.loc[j,'covalent_rad'] = np.nan
                try:
                    df3.loc[j,'covalent_rad_emp'] = float(df4.loc[j,'atom_radii']
                                                          ['Covalent radius (empirical)'].strip())
                except ValueError:
                    df3.loc[j,'covalent_rad_emp'] = np.nan
                try:
                    df3.loc[j,'van_der_waals_rad'] = float(df4.loc[j,'atom_radii']['van der Waals radius'].strip())
                except ValueError:
                    df3.loc[j,'van_der_waals_rad'] = np.nan
                    
                try:
                    df3.loc[j,'orbital_radii_s'] = float(df4.loc[j,'orbital_radii']
                                                       ['s orbital'].strip())
                except AttributeError:
                    df3.loc[j,'orbital_radii_s'] = np.nan
                try:
                    df3.loc[j,'orbital_radii_p'] = float(df4.loc[j,'orbital_radii']
                                                      ['p orbital'].strip())
                except AttributeError:
                    df3.loc[j,'orbital_radii_p'] = np.nan
                try:
                    df3.loc[j,'orbital_radii_d'] = float(df4.loc[j,'orbital_radii']
                                                          ['d orbital'].strip())
                except AttributeError:
                    df3.loc[j,'orbital_radii_d'] = np.nan
                try:
                    df3.loc[j,'orbital_radii_f'] = float(df4.loc[j,'orbital_radii']['f orbital'].strip())
                except AttributeError:
                    df3.loc[j,'orbital_radii_f'] = np.nan
                    
                try:
                    df3.loc[j,'boiling_point'] = df4.loc[j,'thermo_chem']['Boiling point']
                except:
                    df3.loc[j,'boiling_point'] = np.nan
                try:
                    df3.loc[j,'supercond_temp'] = df4.loc[j,'thermo_chem']['Superconduction temperature']
                except:
                    df3.loc[j,'supercond_temp'] = np.nan
                try:
                    df3.loc[j,'melting_point'] = df4.loc[j,'thermo_chem']['Melting point']
                except:
                    df3.loc[j,'melting_point'] = np.nan
                try:
                    df3.loc[j,'liquid_range'] = df4.loc[j,'thermo_chem']['Liquid range']
                except:
                    df3.loc[j,'liquid_range'] = np.nan
                try:
                    df3.loc[j,'critical_temperature'] = df4.loc[j,'thermo_chem']['Critical temperature']
                except:
                    df3.loc[j,'critical_temperature'] = np.nan
                
                    
                
                try:
                    df3.loc[j,'Electron_bin_En_1s'] = float(df4.loc[j,'Electron_bin_En']['1s'])
                except KeyError:
                    df3.loc[j,'Electron_bin_En_1s'] = np.nan
                try:
                    df3.loc[j,'Electron_bin_En_2s'] = float(df4.loc[j,'Electron_bin_En']['2s'])
                except KeyError:
                    df3.loc[j,'Electron_bin_En_2s'] = np.nan
                try:
                    df3.loc[j,'Ionisation_En_1st'] = float(df4.loc[j,'Ionisation_En']['1st'])
                except KeyError:
                    df3.loc[j,'Ionisation_En_1st'] = np.nan
                try:
                    df3.loc[j,'Ionisation_En_2nd'] = float(df4.loc[j,'Ionisation_En']['2nd'])
                except KeyError:
                    df3.loc[j,'Ionisation_En_2nd'] = np.nan
                try:
                    df3.loc[j,'Ionisation_En_3rd'] = float(df4.loc[j,'Ionisation_En']['3rd'])
                except KeyError:
                    df3.loc[j,'Ionisation_En_3rd'] = np.nan
                
    globals()['dfwebele'] = df3

path = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))

def heatmap(Spin = '', method = ''):
    '''
After importing periodictable module one must call this heatmap method to get interactive heatmaps. One must supply Spin and method appropirate keywords agruments as per one's interest. 

Spin arg value could be :

* "True"

* "False"

method arg value could be :

* "hse06"

* "pbe"

* "pbe0"

* "pbesol"

* "pw-lda"

* "revpbe"

* "matmin"

* "lda2015"

* "pymat"

* "webele"

.. note::
   Note : for method “matmin”/“lda2015”/“pymat”/"webele" spin arg value has to "False"

As per different combinations specific element features will be loaded in heatmap plots.
    '''
    if Spin.lower() == 'false' and method.lower() == 'hse06':
        clean();
        path_new = os.path.join(path, "data","dft", "no_spin_hse06_really_tight.csv")
        df4 = pd.read_csv(path_new)
        df4.set_index('atomic_element_symbol',inplace=True)
        f(df3,df4)
        Feature = featuref()
        showfigf(Feature)
    elif Spin.lower() == 'false' and method.lower() =='pbe':
        clean();
        path_new = os.path.join(path, "data","dft", "no_spin_pbe_really_tight.csv")
        df4 = pd.read_csv(path_new)
        df4.set_index('atomic_element_symbol',inplace=True)
        f(df3,df4)
        Feature = featuref()
        showfigf(Feature)
    elif Spin.lower() == 'false' and method.lower() =='pbe0':
        clean();
        path_new = os.path.join(path, "data","dft", "no_spin_pbe0_really_tight.csv")
        df4 = pd.read_csv(path_new)
        df4.set_index('atomic_element_symbol',inplace=True)
        f(df3,df4)
        Feature = featuref()
        showfigf(Feature)
    elif Spin.lower() == 'false' and method.lower() =='pbesol':
        clean();
        path_new = os.path.join(path, "data","dft", "no_spin_pbesol_really_tight.csv")
        df4 = pd.read_csv(path_new)
        df4.set_index('atomic_element_symbol',inplace=True)
        f(df3,df4)
        Feature = featuref()
        showfigf(Feature)
    elif Spin.lower() == 'false' and method.lower() =='pw-lda':
        clean();
        path_new = os.path.join(path, "data","dft", "no_spin_pw-lda_really_tight.csv")
        df4 = pd.read_csv(path_new)
        df4.set_index('atomic_element_symbol',inplace=True)
        f(df3,df4)
        Feature = featuref()
        showfigf(Feature)
    elif Spin.lower() == 'false' and method.lower() =='revpbe':
        clean();
        path_new = os.path.join(path, "data","dft", "no_spin_revpbe_really_tight.csv")
        df4 = pd.read_csv(path_new)
        df4.set_index('atomic_element_symbol',inplace=True)
        f(df3,df4)
        Feature = featuref()
        showfigf(Feature)
    elif Spin.lower() == 'true' and method.lower() =='hse06':
        clean();
        path_new = os.path.join(path, "data","dft", "spin_hse06_really_tight.csv")
        df4 = pd.read_csv(path_new)
        df4.set_index('atomic_element_symbol',inplace=True)
        t(df3,df4)
        Feature = featuret()
        showfigt(Feature)
    elif Spin.lower() == 'true' and method.lower() =='pbe':
        clean();
        path_new = os.path.join(path, "data","dft", "spin_pbe_really_tight.csv")
        df4 = pd.read_csv(path_new)
        df4.set_index('atomic_element_symbol',inplace=True)
        t(df3,df4)
        Feature = featuret()
        showfigt(Feature)
    elif Spin.lower() == 'true' and method.lower() =='pbe0':
        clean();
        path_new = os.path.join(path, "data","dft", "spin_pbe0_really_tight.csv")
        df4 = pd.read_csv(path_new)
        df4.set_index('atomic_element_symbol',inplace=True)
        t(df3,df4)
        Feature = featuret()
        showfigt(Feature)
    elif Spin.lower() == 'true' and method.lower() =='pbesol':
        clean();
        path_new = os.path.join(path, "data","dft", "spin_pbesol_really_tight.csv")
        df4 = pd.read_csv(path_new)
        df4.set_index('atomic_element_symbol',inplace=True)
        t(df3,df4)
        Feature = featuret()
        showfigt(Feature)
    elif Spin.lower() == 'true' and method.lower() =='pw-lda':
        clean();
        path_new = os.path.join(path, "data","dft", "spin_pw-lda_really_tight.csv")
        df4 = pd.read_csv(path_new)
        df4.set_index('atomic_element_symbol',inplace=True)
        t(df3,df4)
        Feature = featuret()
        showfigt(Feature)
    elif Spin.lower() == 'true' and method.lower() =='revpbe':
        clean();
        path_new = os.path.join(path, "data","dft", "spin_revpbe_really_tight.csv")
        df4 = pd.read_csv(path_new)
        df4.set_index('atomic_element_symbol',inplace=True)
        t(df3,df4)
        Feature = featuret()
        showfigt(Feature)
    elif Spin.lower() == 'false' and method.lower() =='lda2015':
        clean();
        path_new = os.path.join(path, "data","lda2015", "Atomic_features.csv")
        df4 = pd.read_csv(path_new)
        df4.set_index('atomic_element_symbol',inplace=True)
        lda(df3,df4)
        Feature = featurelda()
        showfiglda(Feature)
    elif Spin.lower() == 'false' and method.lower() =='matmin':
        clean();
        df4 = mm.df
        df4.set_index('atomic_element_symbol',inplace=True)
        matmin(df3,df4)
        Feature = featurematmin()
        showfigmatmin(Feature)
    elif Spin.lower() == 'false' and method.lower() =='pymat':
        clean();
        df4 = pymatdf.df_clean
        df4.set_index('Element Symbol',inplace=True)
        pymat(df3,df4)
        Feature = featurepymat()
        showfigpymat(Feature)
    elif Spin.lower() == 'false' and method.lower() =='webele':
        clean();
        df4 = apwebele.df_comb
        webele(df3,df4)
        Feature = featurewebele()
        showfigwebele(Feature)
    else:
        raise Exception("Please check the input parameters or Data for specified functional and spin setting is not available")

